const EventEmitter = require('events');
const clone = require('clone');
const diff = require("deep-object-diff").diff;


const deepmerge = require("deepmerge");
//const { encode } = require('punycode');
const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;

class VehicleStateServer extends EventEmitter {
    constructor(socketIOSocket, expressApp, logger, identifiers) {
        super();
        this.io = socketIOSocket || null;
        this.app = expressApp || null;
        this.pushState = new Map();
        this.encodedPushState = new Map();
        this.pullState = new Map();
        this.clients = new Map();
        this.vehicleSockets = new Map();
        this.idMap = new Map();
        this.transmittQueue = [];
        this.sequenceNumber = 1;
        this.paused = false;
        this.currentMessage = null
        this.updateRecord = "S§0";
        if (identifiers) {
            identifiers.forEach((identifier) => {
                _mapIdentifier(this, identifier);
            });
        }


        if (logger) this.logger = (type, message) => {
            if ((type == "error") || (type == "warn") || (type == "info")) logger[type](message);
            else throw ("Invalid log type (error,warn or info)");
            if (type == "error") throw (message);
        }
        else this.logger = (type, message) => {
            if ((type == "error") || (type == "warn") || (type == "info")) console.log(type + ": " + message);
            else throw ("Invalid log type (error,warn or info)");
            if (type == "error") throw (message);
        };

        let self = this;
        /*if (this.app) {
            expressApp.get('/api/vehicles/:vehicleRef', async (req, res) => {
                const vehicle = this.pullState.get(req.params.vehicleRef);
                res.json(vehicle);
            })

            expressApp.get('/api/vehiclesencoded/', async (req, res) => {
                const result = _getEncodedState(self);
                res.send(result);
            })
        }*/

        if (this.io) {

            this.io.on('connection', (socket) => {
                const socketId = socket.id;
                self.logger("info", 'A user connected: ' + socketId)

                const clientRecord = {
                    socketId: socketId,
                    socket: socket,
                    userId: null,
                    bbox: [[0, 180][0, 180]],
                    selectedVehicle: null
                }

                self.clients.set(socketId, clientRecord);

                if (!self.paused) socket.emit('updates', _getEncodedState(self));
                else socket.emit('message', this.currentMessage);

                socket.on('disconnect', (reason) => {
                    //const clientId=socket.id;
                    self.logger("info", 'A user disconnected: ' + clientRecord.userId + " SocketID: " + socketId)
                    self.clients.delete(socketId);
                });


                socket.on('restart', function (message) {
                    self.logger("info", "User requested restart");
                    if (!self.paused) socket.emit('updates', _getEncodedState(self));
                });
                socket.on('changeView', function (bbox) {
                    self.logger("info", "User changed view: " + clientRecord.userId + " > " + JSON.stringify(bbox));
                    clientRecord.bbox = bbox;

                });
                socket.on('userId', function (userId) {
                    self.logger("info", "User sent userId: " + userId);
                    clientRecord.userId = userId;

                });
                socket.on('vehicle', function (vehicleRef) {
                    if (vehicleRef) {
                        let vehicleSockets = self.vehicleSockets.get(vehicleRef);
                        if (!vehicleSockets) vehicleSockets = new Map();
                        vehicleSockets.set(socket.id, socket);
                        self.vehicleSockets.set(vehicleRef, vehicleSockets);
                        self.logger("info", "User selected vehicle: " + clientRecord.userId + " > " + vehicleRef);
                        clientRecord.selectedVehicle = vehicleRef;
                        _sendVehicle(self, socket, vehicleRef);
                    } else {
                        self.vehicleSockets.forEach((vehicleSockets, key) => {
                            if (vehicleSockets.has(socket.id)) {
                                vehicleSockets.delete(socket.id);
                                self.vehicleSockets.set(vehicleRef, vehicleSockets);
                                clientRecord.selectedVehicle = null;
                                self.logger("info", "User unselected vehicle: " + clientRecord.userId + " > " + key);
                            }
                        })
                    }
                    //socket.emit('vehicle', );
                });
            })
        }


        setInterval(() => {

            if (self.updateRecord.length > 4) {
                //if (self.transmittQueue.length > 0) {
                if (self.sequenceNumber == 100) self.sequenceNumber = 0;
                //let records = "S§" + self.sequenceNumber;

                //self.transmittQueue.forEach((record) => {
                //    records = records + "\n" + record;
                //});
                //self.transmittQueue = [];
                if (self.io && !self.paused) self.io.emit('updates', self.updateRecord);
                //else console.log(records);

                //_getEncodedState(self)
                self.updateRecord = "S§" + self.sequenceNumber;
                self.sequenceNumber++;
            }
        }, 250);

    }

    pause(message) {
        this.currentMessage = message;
        this.paused = true;
        this.io.emit('message', this.currentMessage);
    }

    resume() {
        this.paused = false;
        this.currentMessage = null
        this.io.emit('reload', true);
    }

    // Types "PUSH","PULL","BOTH"
    async updateState(type, key, record) {

        if ((type == "PULL") || (type == "PUSH") || (type == "BOTH")) {
            if ((type == "PUSH") || (type == "BOTH")) {
                let oldRecord = _getState(this, this.pushState, key);
                if (!oldRecord) oldRecord = {};
                const newRecord = deepmerge(oldRecord, record, { arrayMerge: overwriteMerge });

                const changes = diff(oldRecord, record);
                let encodedChanges = null;
                if (Object.keys(changes).length !== 0) {
                    encodedChanges = _encodeRecord(this, key, changes, "D");
                    if (encodedChanges) this.updateRecord = this.updateRecord + "\n" + encodedChanges;
                    //this.transmittQueue.push(encodedChanges);
                }

                _setState(this, this.pushState, key, newRecord)
                this.encodedPushState.set(key, _encodeRecord(this, key, newRecord, "R"));

            }
            if ((type == "PULL") || (type == "BOTH")) {
                let oldRecord = _getState(this, this.pullState, key);
                if (!oldRecord) oldRecord = {};
                const newRecord = deepmerge(oldRecord, record, { arrayMerge: overwriteMerge });
                _setState(this, this.pullState, key, newRecord);
                const vehicleSockets = this.vehicleSockets.get(key);
                if (vehicleSockets)
                    vehicleSockets.forEach(socket => {
                        _sendVehicle(this, socket, key);
                    })
                //this.transmittQueue.push("U§" + _mapIdentifier(this, key));
                //this.logger("info", JSON.stringify(newRecord));
            }
        } else {
            this.logger("error", "Invalid update type (PULL, PUSH or BOTH)");
            throw ("Invalid update type (PULL, PUSH or BOTH)");
        }
    }


    getState(type, key) {
        if (type.toUpperCase() == "PULL") {
            return _getState(this, this.pullState, key);
        } else if (type.toUpperCase() == "PUSH") {
            return _getState(this, this.pushState, key);
        }
    }
}

function _sendVehicle(context, socket, vehicleRef) {
    const record = _getState(context, context.pullState, vehicleRef);
    if (record)
        if (!context.paused) socket.emit('vehicle', { key: vehicleRef, state: record });
}

function _getEncodedState(context) {
    let result = "S§-1";
    context.idMap.forEach((index, key) => {
        result = result + "\nF§" + index + "^" + key;
    })
    context.encodedPushState.forEach((record) => {
        result = result + "\n" + record
    })
    return result;
}

function _encodeRecord(context, key, record, prefix) {
    const shortKey = _mapIdentifier(context, key);

    //let _encodeRecord=encodedKey+":";
    const messagePrefix = prefix || "R";

    let encodedFields = 0
    let result = prefix + "§" + shortKey + "^";
    Object.entries(record).forEach(([fieldKey, value]) => {
        if (value !== undefined) {
            if (Array.isArray(record[fieldKey])) throw ("Arrays are not permitted in push state. Only Numbers, Boolean and String values");
            if ((!value) && (value !== 0) && (value !== false)) value = null;
            const fieldId = _mapIdentifier(context, fieldKey);
            let recordSeparator = "|";
            if (encodedFields == 0) recordSeparator = "";
            result = result + recordSeparator + fieldId + "~" + value;
            encodedFields++;
        }
    });
    if (!encodedFields) result = null;
    //console.log(result);
    return result;
}

function _mapIdentifier(context, identifier) {
    let map = context.idMap;
    if (map && identifier) {
        let index = map.size;
        if (!map.has(identifier)) {
            _setState(context, map, identifier, index);
            this.updateRecord = this.updateRecord + "\n" + "F§" + index + "^" + identifier;
            //context.transmittQueue.push("F§" + index + "^" + identifier);
        } else {
            index = map.get(identifier);
        }
        return index;
    } else {
        context.logger("error", "Missing parameters in _mapIdentifier (map or identifier)");
        throw ("Missing parameters in _mapIdentifier (map or identifier)");
    }
}

function _getState(context, state, key) {
    if (state && key) {
        if (!state.has(key)) _setState(context, state, key, {});
        return clone(state.get(key));
    } else {
        context.logger("error", "Missing parameters in _getState (state or key)");
        throw ("Missing parameters in _getState (state or key)");
    }

}

function _setState(context, state, key, record) {
    if (context && state && key && (record !== null) && (record !== undefined)) state.set(key, record);
    else {
        context.logger("error", "Missing parameters in _setState (state, key or record)");
        throw ("Missing parameters in _setState (state, key or record)");
    }
}


module.exports = VehicleStateServer;