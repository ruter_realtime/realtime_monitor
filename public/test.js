function updateFeature(vehicleRef, change) {
    var feature = vectorSource.getFeatureById(vehicleRef);
    let location=change.location;
    if(change.location) delete change.location;
    if (!feature) {
        if(location){
            var position = geohash.decode(location);
            feature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([position.longitude, position.latitude])));
            feature.setId(vehicleRef);
            //feature.setStyle(vehicleTypeStyle);
            feature.setProperties(change);
            vectorSource.addFeature(feature);
            feature = vectorSource.getFeatureById(vehicleRef);
        }
    } else {
        feature.setProperties(change);
    }

    if (location) {
        var position = geohash.decode(location);
        var newPosition = new ol.geom.Point(ol.proj.fromLonLat([position.longitude, position.latitude]));
        feature.setGeometry(newPosition);
        //window;
    }
}

const OSMLayer = new ol.layer.Tile({
    source: new ol.source.XYZ({
        url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    })
})

const vectorSource = new ol.source.Vector({
    features: []

});

var vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    //style: markerStyleFunction
});



function centerMap(map, pos) {
    //let loc = ol.proj.fromLonLat(pos);
    let view = map.getView();
    view.setCenter(pos);
}



const map = new ol.Map({
    target: 'map',
    layers: [OSMLayer, vectorLayer],
    //controls: DefaultControls({ attribution: false }).extend([attribution]),
    view: new ol.View({
        zoom: 13,
        maxZoom: 19,
        enableRotation: false
    })
});


centerMap(map, ol.proj.fromLonLat([10.732747, 59.910946]));

var stroke = new ol.style.Stroke({ color: 'black', width: 2 });
var fill = new ol.style.Fill({ color: 'red' });

var vehicleTypeStyle = new ol.style.Circle({
    radius: 10,
    fill: fill,
    stroke: stroke
});

function markerStyleFunction(feature, resolution) {

    let styleArray = [vehicleTypeStyle]


    return styleArray;

}

const socket = io.connect();

let vehicleState = null;
let lookupList = null;
let lastSequenceNumber = null;
let byteCounter = 0;

setInterval(() => {
    console.log("Bandwidth: " + ((byteCounter / 3) / 1024).toFixed(1) + " kb/s")
    byteCounter = 0;
}, 3000)

socket.on('updates', (records) => {
    //console.log("Updates: ");
    const byteSize = str => new Blob([str]).size;

    byteCounter = byteCounter + byteSize(records);
    const updates = records.split("\n");

    const sequenceRecord = updates.shift();
    const sequenceNumber = sequenceRecord.split("§")[1];
    if (sequenceNumber == -1) {
        lastSequenceNumber = null;
        vehicleState = null;
        //lookupList = null;
    }
    //console.log(sequenceNumber);
    lastSequenceNumber++;
    if (lastSequenceNumber == 100) lastSequenceNumber = 0;
    if (sequenceNumber)

        if (!vehicleState) vehicleState = new Map();


    updates.forEach(record => {
        const splitRecord = record.split("§");
        const command = splitRecord.shift();

        const contentSplit = splitRecord.shift().split("^");
        const id = contentSplit.shift();
        const value = contentSplit.shift();
        if (command == "F") addLookup(id, value);
        else if (command == "D") updateState(id, value);
        else if (command == "R") updateState(id, value);

        //console.log(value);
    })

    function updateState(index, value) {
        const key = lookup(index);
        const record = decodeRecord(value);

        if (!vehicleState.has(key)) vehicleState.set(key, {});

        const vehicle = vehicleState.get(key);
        Object.entries(record).forEach(([fieldKey, value]) => {
            vehicle[fieldKey] = value;
        });

        updateFeature(key, record);


        /*if (vehicle.type == "TRAM")
            console.log(key + "/" + JSON.stringify(vehicle));*/
    }

    function decodeRecord(records) {
        const keyValuePairs = records.split("|");
        const result = {};
        keyValuePairs.forEach((record) => {
            const split = record.split("~");
            const field = lookup(split[0]);
            let value = split[1];
            if (!isNaN(value)) value = Number(value);
            else if (value == "null") value = null;
            else if (value == "true") value = true;
            else if (value == "false") value = false;
            result[field] = value;
        })
        return result
    }

    function lookup(index) {
        return lookupList.get(index);
    }

    function addLookup(index, key) {
        if (!lookupList) lookupList = new Map();
        lookupList.set(index, key);
    }
});


