const sampleGeoJSON = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "fillColor": "#FF00007F",
        "pointRadius": 15,
        "label": "HEI"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [10.748147964477539, 59.914074042820715]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "strokeColor": "black",
        "strokeWidth": 5,
        "strokeType": "DASH",
        "label": "HEI"
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [[10.748147964477539, 59.914074042820715], [10.748577117919922, 59.91183657536122], [10.757675170898438, 59.91050262877672], [10.760936737060547, 59.90727511694244], [10.750551223754883, 59.90490807552136], [10.738191604614258, 59.90843705734661], [10.737762451171875, 59.911965664121674]]
      }
    }, {
      "type": "Feature", "properties": {
        "fillColor": "#00FF007F",

      }, "geometry": {
        "type": "Polygon", "coordinates": [[[10.745658874511719, 59.921086665516114], [10.74085235595703, 59.91897873224253], [10.755958557128906, 59.915364820832544], [10.757589340209961, 59.920011206085576], [10.752096176147461, 59.922549234410766], [10.745658874511719, 59.921086665516114]]]
      }
    }
  ]
};


//Function to retieve a browser cookie
function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

//Function to set a browser cookie
function setCookie(name, value, options = {}) {
  options = {
    path: '/',
    // add other defaults here if necessary
    ...options
  };
  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }
  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }
  document.cookie = updatedCookie;
}

//Function to generate a random ID
function randomId() {
  const uint32 = window.crypto.getRandomValues(new Uint32Array(1))[0];
  return uint32.toString(16);
}

// Handle unique user ID
let userId = getCookie("ID"); //Get user ID from cookie
if (!userId) { //Generate User ID if none exists.
  userId = randomId();
  setCookie("ID", userId, { 'max-age': 14 * 24 * 3600 }) //Set User Id Cookie with 14 days timeout
  console.log("New userId: " + userId);
} else console.log("UserId: " + userId);


//Setup OpenLayer classes
const Circle = ol.geom.Circle;
const RegularShape = ol.style.RegularShape;
const Feature = ol.Feature;
const Point = ol.geom.Point;
const VectorSource = ol.source.Vector;
const VectorLayer = ol.layer.Vector;
const Style = ol.style.Style;
const CircleStyle = ol.style.Circle;
const PolygonStyle = ol.style.Polygon;
const TextStyle = ol.style.Text;
const Fill = ol.style.Fill;
const Stroke = ol.style.Stroke;
const Icon = ol.style.Icon;
const Select = ol.interaction.Select;
const Color = ol.color;
const LineString = ol.geom.LineString;
const Attribution = ol.control.Attribution;
const DefaultControls = ol.control.defaults;
const GeoJSON = ol.format.GeoJSON;
const getBottomLeft = ol.extent.getBottomLeft;
const getTopRight = ol.extent.getTopRight;
const toLonLat = ol.proj.toLonLat;

//Create WS
const socket = io();


let map;
let vectorSource;
let journeyPatternSource;
let geoJSONSource;
let selectedVehicleKey;
let selectedVehicleData;
let selectedVehicles = [];
const isTouchDevice = (navigator.maxTouchPoints > 1);

const gnssSources = ["ITXPT", "SIS", "VIX", "NOBINA", "PILOTFISH", "FALTCOM"];

const textColor = new Fill({
  color: 'black'
});
const textBackgroundColor = new Fill({
  color: colorWithAlpha('white', 0.5)
});

var vehicleFilter = "ALL";
var statusFilter = "ALL";

function setFilter(type, value) {
  if (type == "vehicle") vehicleFilter = value;
  else if (type == "status") statusFilter = value;
  vectorSource.changed();
}


function colorWithAlpha(color, alpha) {
  const [r, g, b] = Array.from(Color.asArray(color));
  return Color.asString([r, g, b, alpha]);
}

let lastBbox = [[null, null], [null, null]];

function initMap() {

  //Collaps/minimize atribution
  const attribution = new Attribution({
    collapsible: true
  });




  function computeQuadKey(x, y, z) {
    var quadKeyDigits = [];
    for (var i = z; i > 0; i--) {
      var digit = 0;
      var mask = 1 << (i - 1);

      if ((x & mask) != 0)
        digit++;
      if ((y & mask) != 0)
        digit = digit + 2;

      quadKeyDigits.push(digit);
    }
    return quadKeyDigits.join('');
  };

  var bingTrafficSource = new ol.source.XYZ({
    //minZoom: 12,
    //maxZoom: 14,
    tileUrlFunction(tileCoord, pixelRatio, projection) {
      var z = tileCoord[0];
      var x = tileCoord[1];
      var y = -tileCoord[2] - 1;
      //return "https://t0.tiles.virtualearth.net/tiles/t" + computeQuadKey(x, y, z) + ".png";

      return "http://trafficrenderer-prod.trafficmanager.net/comp/ch/" + computeQuadKey(x, y, z)
    },
    attributions: 'Traffic layer © <a href="https://www.bing.com/maps">Bing</a> '
  });



  var bingTrafficLayer = new ol.layer.Tile({
    source: bingTrafficSource,
    maxResolution: 200,
    preload: 12
  });

  var arcgisSatLayer = new ol.layer.Tile({
    source: new ol.source.XYZ({

      attributions: 'Orthophoto © <a href="https://services.arcgisonline.com/ArcGIS/' +
        'rest/services/World_Topo_Map/MapServer">ArcGIS</a> ',
      url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
        'World_Imagery/MapServer/tile/{z}/{y}/{x}'
    }),
    preload: 12,
    //minZoom: 12,
    //maxZoom: 19,
  })


  var _url = "http://opencache.statkart.no/gatekeeper/gk/gk.open?";

  //Start: source
  var sourceWMSC = new ol.source.TileWMS({
    url: _url,
    params: {
      LAYERS: 'norges_grunnkart',
      VERSION: '1.1.1'
    }
  });
  //End: source

  //Start: layer
  var tileLayerWMSC = new ol.layer.Tile({
    title: "Norges grunnkart",
    source: sourceWMSC,
    //preload: 12
  });


  var tileLayerOSM = new ol.layer.Tile({
    preload: 12,
    source: new ol.source.OSM()
  });

  const urlParams = new URLSearchParams(window.location.search);
  let showLayers = [];
  //showLayers.push(tileLayerWMSC);
  if (urlParams.get("search")) document.getElementById("searchBox").value = urlParams.get("search");


  if (urlParams.get("satellite") == "true") showLayers.push(arcgisSatLayer);
  else showLayers.push(tileLayerOSM);
  if (urlParams.get("traffic") == "true") {
    showLayers.push(bingTrafficLayer);
    setInterval(() => {
      console.log("Refreshing traffic layer");
      let source = bingTrafficLayer.getSource();
      source.tileCache.expireCache({});
      source.tileCache.clear();
      source.refresh({ force: true });
    }, 180000);
  }

  function centerMap(pos) {
    //let loc = ol.proj.fromLonLat(pos);
    let view = map.getView();
    view.setCenter(pos);
  }

  let zoomLevel = getCookie("zoom");
  if (!zoomLevel) {
    zoomLevel = 13;
    setCookie("zoom", zoomLevel, { 'max-age': 14 * 24 * 3600 })
  }



  map = new ol.Map({
    target: 'map',
    layers: showLayers,
    controls: DefaultControls({ attribution: false }).extend([attribution]),
    view: new ol.View({
      //center: ol.proj.fromLonLat([10.732747, 59.910946]),
      zoom: zoomLevel,
      //minZoom: 11,
      maxZoom: 19,
      enableRotation: false
    })
  });

  let centerCookie = getCookie("center");
  let center = [10.732747, 59.910946];
  if (centerCookie) {
    const decodedHash = geohash.decode(centerCookie);
    center = [decodedHash.longitude, decodedHash.latitude];
  } else {
    setCookie("center", geohash.encode(center[1], center[0], 11), { 'max-age': 14 * 24 * 3600 })
  }

  centerMap(ol.proj.fromLonLat(center))



  geoJSONSource = new ol.source.Vector({
    //features: new GeoJSON().readFeatures(sampleGeoJSON, {featureProjection: 'EPSG:3857'})
    features: []
  });

  var geoJSONLayer = new VectorLayer({
    source: geoJSONSource,
    style: geoJSONStyleFunction
  });
  map.addLayer(geoJSONLayer);

  vectorSource = new VectorSource({
    features: []

  });

  var vectorLayer = new VectorLayer({
    source: vectorSource,
    style: vehicleStyleFunction
  });

  map.addLayer(vectorLayer);


  var selectSingleClick = new Select({
    style: vehicleStyleFunction
  });
  map.addInteraction(selectSingleClick);
  selectSingleClick.on('select', function (e) {

    if (e.selected[0]) {
      selectedVehicleKey = e.selected[0].id_;
      selectedVehicleData = {};
      socket.emit('vehicle', null);
      socket.emit('vehicle', selectedVehicleKey);
      console.log("Selected vehicle: " + selectedVehicleKey);
    }
    else {
      selectedVehicleKey = null;
      selectedVehicleData = null;
      socket.emit('vehicle', null);
    }
  });

  //var status = document.getElementById('statusText');
  var selected = null;

  map.on('pointermove', function (e) {
    if (selected !== null) {
      selected.forEach(feature => {
        feature.setProperties({ isHovering: false });
      });
      selected = null;
    }


    map.forEachFeatureAtPixel(e.pixel, (feature) => {
      if (!selected) selected = [];
      selected.push(feature);
      feature.setProperties({ isHovering: true });
      //f.setStyle(highlightStyle);
      return true;
    });

    map.on('moveend', (evt) => {
      const map = evt.map;
      const currentZoom = map.getView().getZoom();
      const cookieZoom = getCookie("zoom");
      if (currentZoom !== cookieZoom) {
        setCookie("zoom", currentZoom, { 'max-age': 14 * 24 * 3600 })
      }

      const currentCenter = toLonLat(map.getView().getCenter());
      const currentGeohash = geohash.encode(currentCenter[1], currentCenter[0], 11)
      const cookieGeohash = getCookie("center");
      if (currentGeohash !== cookieGeohash) {
        setCookie("center", currentGeohash, { 'max-age': 14 * 24 * 3600 })
      }

      //setCookie("center", geohash.encode(center[1],center[0],11), {'max-age': 14*24*3600})

      const extent = map.getView().calculateExtent(map.getSize());
      const bottomLeft = toLonLat(getBottomLeft(extent));
      const topRight = toLonLat(getTopRight(extent));

      let bbox = [[bottomLeft[0], bottomLeft[1]], [topRight[0], topRight[1]]];
      if (lastBbox[0][0] !== bbox[0][0] ||
        lastBbox[0][1] !== bbox[0][1] ||
        lastBbox[1][0] !== bbox[1][0] ||
        lastBbox[1][1] !== bbox[1][1]) {
        console.log(JSON.stringify(bbox));
        socket.emit('changeView', bbox);
        lastBbox = bbox;
      }

    });

  });

}

let lastMove = 0;

// GeoJSON feature renderer
function geoJSONStyleFunction(feature, resolution) {
  const returnStyles = [];
  const featureType = feature.getGeometry().getType();
  const featureProperties = feature.getProperties();

  let pointRadius = 10;
  if (featureProperties.pointRadius) pointRadius = featureProperties.pointRadius;
  let fillColor = "black";
  if (featureProperties.fillColor) fillColor = featureProperties.fillColor;
  let strokeColor = "black";
  if (featureProperties.strokeColor) strokeColor = featureProperties.strokeColor;
  let strokeWidth = 1;
  if (featureProperties.strokeWidth) strokeWidth = featureProperties.strokeWidth;
  let strokeType = null;
  if (featureProperties.strokeType === "DOT") strokeType = [.1 * strokeWidth, 5 * strokeWidth];
  else if (featureProperties.strokeType === "DASH") strokeType = [5 * strokeWidth, 5 * strokeWidth];
  let label = null;
  if (featureProperties.label) {
    let yOffset = 0;
    if (featureType === "Point") yOffset = pointRadius + 13;
    label = new TextStyle({
      fill: textColor,
      textAlign: 'center',
      scale: 1.3,
      offsetY: yOffset,
      //offsetX: -52 * scale * feature_scale,
      text: featureProperties.label,
      backgroundFill: textBackgroundColor,
      padding: [3, 3, 0, 3]
    });
  }

  if (featureType === "Point") {
    returnStyles.push(
      new Style({
        zIndex: 0,
        image: new CircleStyle({
          radius: pointRadius,
          fill: new Fill({
            color: fillColor
          }),
        }),
        text: label
      })
    );
  } else if (featureType === "LineString") {
    returnStyles.push(
      new ol.style.Style({
        zIndex: 0,
        stroke: new ol.style.Stroke({
          color: strokeColor,
          width: strokeWidth,
          lineDash: strokeType
        }),
        text: label
      })
    );
  } else if (featureType === "Polygon") {
    returnStyles.push(
      new ol.style.Style({
        zIndex: 0,
        fill: new Fill({
          color: fillColor
        }),
        text: label
      })
    );
  }
  return returnStyles;
}

//Vehicle feature renderer
function vehicleStyleFunction(feature, resolution) {

  const vehicleRef = feature.id_;
  let isFilteredVehicle = null;
  if (selectedVehicles.length) {
    isFilteredVehicle = selectedVehicles.includes(vehicleRef);
    if (selectedVehicles.includes(vehicleRef) === false) return [];
  }

  const isHidden = feature.get("isHidden");
  if (isHidden && isFilteredVehicle !== true) return [];

  let hasSelectedVehicle = false;
  let isSelectedVehicle = false;
  if (selectedVehicleKey) {
    hasSelectedVehicle = true;
    if (selectedVehicleKey == vehicleRef) isSelectedVehicle = true;
  }

  //Set default text bgcolor
  let textBackgroundColor = new Fill({
    color: colorWithAlpha('white', 0.5)
  });

  //Vehicle label default setup
  const externalDisplayCode = feature.get("externalDisplayCode");
  const externalDisplayText = feature.get("externalDisplayText");
  const externalDisplaySubText = feature.get("externalDisplaySubText");

  let labelText = null;
  let showLabel = true;
  let longLabelText = "Ikke i trafikk"
  if (externalDisplayText) {
    longLabelText = "";
    if (externalDisplayCode) longLabelText = longLabelText + externalDisplayCode + " ";
    longLabelText = longLabelText + externalDisplayText;
    if (externalDisplaySubText) longLabelText = longLabelText + "\n" + externalDisplaySubText;
  }
  let shortLabelText = null;
  if (externalDisplayCode) shortLabelText = externalDisplayCode.toString();
  let textScale = 1;

  const isHovering = feature.get("isHovering");

  if (isHovering) textBackgroundColor = new Fill({
    color: colorWithAlpha('white', 0.9)
  });

  //Vehicle Notification Icon default setup
  let showNotificationIcon = false;
  let notificationIconSource = null
  let notificationIconScale = 1;
  let notificationText = null;


  //Vehicle dot notification icon setup
  const gnssStatus = feature.get("gnssStatus");
  const stopSignal = feature.get("stopSignal");


  if (gnssStatus === "OCCASIONAL") {
    notificationIconSource = "coverage-yellow.svg"
    notificationText = "Only occasional position (SIS)"
    showNotificationIcon = true;

  }
  else if (gnssStatus === "NO_COVER") {
    notificationIconSource = "coverage-orange.svg"
    notificationText = "GPS Lost coverage"
    showNotificationIcon = true;
  }
  else if (gnssStatus === "LOST_CONNECTION") {
    notificationIconSource = "coverage-orange.svg"
    notificationText = "Lost position feed"
    showNotificationIcon = true;
  }
  else if (gnssStatus === "DEFECTIVE") {
    notificationIconSource = "coverage-red.svg"
    notificationText = "GPS defective"
    showNotificationIcon = true;
  }
  else if (gnssStatus === "OFFLINE" ) {
    notificationIconSource = "coverage-red.svg"
    notificationText = "GPS Offline"
    showNotificationIcon = true;
  }
  else if (stopSignal === 1) {
    notificationIconSource = "stop.svg"
    notificationText = "Stopping"
    showNotificationIcon = true;
  }

  //Vehicle dot setup
  const dotColor = feature.get("dotColor");

  let feature_scale = 1;
  if (isHovering) feature_scale = 1.3;
  const vehicleTransparency = 0.8;

  let vehicleColor = colorWithAlpha("black", vehicleTransparency);
  if (dotColor) vehicleColor = colorWithAlpha(dotColor.substr(0, 7), vehicleTransparency);
  let showDot = true;

  //Vehicle Icon Setup
  const vehicleIcon = feature.get("vehicleIcon");

  let showIcon = true;
  let vehicleIconFile = "question-mark.svg";
  let vehicleIconScale = 0.03;
  let vehicleIconTransparency = 1;
  if (vehicleIcon == "BUS") {
    vehicleIconFile = "bus.svg";
    vehicleIconScale = 1;
    showIcon = true;
  }
  else if (vehicleIcon == "TRAM") {
    vehicleIconFile = "tram.svg";
    vehicleIconScale = 1.3;
    showIcon = true;
  }
  else if (vehicleIcon == "FERRY") {
    vehicleIconFile = "ferry.svg";
    vehicleIconScale = 1.2;
    showIcon = true;
  }

  //Vehicle Ring setup
  const ringColor = feature.get("ringColor");
  let hasRing = false;
  if (ringColor) hasRing = true;

  //handle selected vehicle
  if (hasSelectedVehicle) {
    //Show details panel
    document.getElementById("overlay").style.display = "block";;


    if (!isSelectedVehicle) {
      // Reuce size of dots if not selected or overing
      if (!isHovering) {
        feature_scale = 0.7;
        showLabel = false;
      }
    } else {

      //normal size of selected and hovering vehicle dots
      feature_scale = 1;


      //Render Details panel
      const currentGnssSource = feature.get("gnssSource");
      const engineType = feature.get("engineType");
      const doorsOpen = feature.get("doorsOpen");
      const assignmentId = feature.get("assignmentId");
      const journeyId = feature.get("journeyId");
      const journeyName = feature.get("journeyName");
      const evCharging = feature.get("evCharging");
      const evStateOfCharge = feature.get("evStateOfCharge");

      if (currentGnssSource && selectedVehicleData.digitalFeatures) {

        let gpsColor = "black";
        if (gnssStatus == "OFFLINE") gpsColor = "darkgray";
        else if (gnssStatus == "NO_COVERAGE") gpsColor = "red";
        else if (gnssStatus == "OCCASIONAL") gpsColor = "orange";
        else if (gnssStatus == "ONLINE") gpsColor = "green";
        document.getElementById("gnss").style.display = "inline";
        gnssSources.forEach((gnssSource) => {
          var typeElement = document.getElementById(gnssSource);
          if (selectedVehicleData.digitalFeatures.position.includes(gnssSource)) {
            typeElement.style.display = "inline";
            if (currentGnssSource === gnssSource) {
              typeElement.style.color = gpsColor;

            } else typeElement.style.color = "darkgray";

          } else {
            typeElement.style.display = "none";
          }

        })
      } else document.getElementById("gnss").style.display = "none";

      var typeElement = document.getElementById("stopSignal");
      if (doorsOpen !== null) {

        typeElement.style.display = "inline";
        if (stopSignal) {
          document.getElementById("stopSignal_on").style.display = "inline";
          document.getElementById("stopSignal_off").style.display = "none";
        }
        else {
          document.getElementById("stopSignal_off").style.display = "inline";
          document.getElementById("stopSignal_on").style.display = "none";
        }
      } else typeElement.style.display = "none";


      var typeElement = document.getElementById("doors");
      if (doorsOpen !== null) {

        typeElement.style.display = "inline";
        if (doorsOpen) {
          document.getElementById("door_open").style.display = "inline";
          document.getElementById("door_closed").style.display = "none";
        }
        else {
          document.getElementById("door_closed").style.display = "inline";
          document.getElementById("door_open").style.display = "none";
        }
      } else typeElement.style.display = "none";

      var typeElement = document.getElementById("evCharging");
      if (evCharging !== null) {

        typeElement.style.display = "inline";
        if (evCharging === "CHARGING") {
          document.getElementById("evCharging_on").style.display = "inline";
          document.getElementById("evCharging_off").style.display = "none";
        }
        else {
          document.getElementById("evCharging_off").style.display = "inline";
          document.getElementById("evCharging_on").style.display = "none";
        }
      } else typeElement.style.display = "none";

      var typeElement = document.getElementById("evStateOfCharge");
      if (evStateOfCharge) {
        typeElement.style.color = "GREEN";
        typeElement.innerText = "SOC:" + evStateOfCharge + "%";
      } else typeElement.innerText = null;

      var typeElement = document.getElementById("engineType");
      if (engineType) {
        typeElement.style.color = "black";
        typeElement.innerText = "(" + engineType.toUpperCase().substr(0, 1) + engineType.toLowerCase().substr(1) + ")";
      } else typeElement.innerText = null;


      const localVehicleId = feature.get("localVehicleId");
      const operatorRef = feature.get("operatorRef");
      if (localVehicleId) document.getElementById("id").innerText = operatorRef.split(":")[2] + " " + localVehicleId;
      else document.getElementById("id").innerText = null;

      const homeDepotName = feature.get("homeDepotName");
      if (homeDepotName) document.getElementById("homeDepot").innerText = homeDepotName;
      else document.getElementById("homeDepot").innerText = null;

      const vin = feature.get("vin");
      if (vin) document.getElementById("vin").innerText = "VIN:" + vin;
      else document.getElementById("vin").innerText = null;

      const registrationNumber = feature.get("registrationNumber");
      if (registrationNumber) document.getElementById("registrationNumber").innerText = "REG:" + registrationNumber;
      else document.getElementById("registrationNumber").innerText = null;

      const sisId = feature.get("sisId");
      if (sisId) document.getElementById("sisId").innerText = "SIS:" + sisId;
      else document.getElementById("sisId").innerText = null;

      const vixId = feature.get("vixId");
      if (vixId) document.getElementById("vixId").innerText = "VIX:" + vixId;
      else document.getElementById("vixId").innerText = null;


      let displayParts = longLabelText.split("\n");
      if (displayParts[0]) document.getElementById("display").innerText = displayParts[0];
      else document.getElementById("display").innerText = null;
      if (displayParts[1]) document.getElementById("subDisplay").innerText = displayParts[1];
      else document.getElementById("subDisplay").innerText = null;

      const externalDisplaySource = feature.get("externalDisplaySource");
      if (externalDisplaySource) document.getElementById("displaySource").innerText = "(" + externalDisplaySource + ")";
      else document.getElementById("displaySource").innerText = null;

      if (assignmentId) {
        document.getElementById("signon").innerText = "";
        document.getElementById("assignmentId").innerText = assignmentId;
        document.getElementById("signonSeparator").innerText = "/";
        document.getElementById("journeyId").innerText = journeyId;
        document.getElementById("journeyName").innerText = " (" + journeyName + ")";
      } else {
        document.getElementById("signon").innerText = "Unknown assignment";
        document.getElementById("assignmentId").innerText = "";
        document.getElementById("signonSeparator").innerText = "";
        document.getElementById("journeyId").innerText = "";
        document.getElementById("journeyName").innerText = "";
      }
    }

    if (selectedVehicleData.geoJSONFeatures) {
      const geoJSONFeatures = new GeoJSON().readFeatures(selectedVehicleData.geoJSONFeatures, { featureProjection: 'EPSG:3857' })
      geoJSONSource.clear();
      geoJSONSource.addFeatures(geoJSONFeatures);
    }
  } else {
    //Clear details panel on deselect
    document.getElementById("overlay").style.display = "none";
    if (document.getElementById("id").innerText) {
      document.getElementById("id").innerText = null;
      document.getElementById("homeDepot").innerText = null;
      document.getElementById("vin").innerText = null;
      document.getElementById("registrationNumber").innerText = null;
      document.getElementById("sisId").innerText = null;
      document.getElementById("vixId").innerText = null;
    }
    //clear geoJSON layer on deselect;
    geoJSONSource.clear();
  }

  //Scaling
  let scale = 0.7;
  if (!isSelectedVehicle) {
    if (resolution > 25 && !isHovering) {
      showLabel = false;
      showIcon = false;
      scale = scale / 4;
      textScale = 0.6;
    }
    else if (resolution > 10 && !isHovering) {
      //showLabel = false;
      if (shortLabelText) labelText = shortLabelText;
      textScale = 0.7;
      scale = scale / 2.5;
    }
    else if (resolution > 8 && !isHovering) {
      if (shortLabelText) labelText = shortLabelText;
      textScale = 0.8;
      scale = scale / 1.5;
    }
    else if (resolution > 4 && !isHovering) {
      if (longLabelText) labelText = longLabelText;
      scale = scale / 1.5;
      textScale = 0.8;
    } else {
      if (longLabelText) labelText = longLabelText;
      scale = scale / 1.5;
      //textScale=0.8;
    }
  } else if (longLabelText) labelText = longLabelText;

  if (isTouchDevice) feature_scale = feature_scale * 1.3;
  let zIndex = 0;
  if (isSelectedVehicle) zIndex = 1;
  else if (isHovering) zIndex = 2;





  //Build vehicle icon
  let vehicleStyle = null;
  if (showIcon) vehicleStyle = new Icon(({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    src: vehicleIconFile,
    scale: scale * vehicleIconScale * feature_scale,
    rotateWithView: false,
    opacity: vehicleIconTransparency
  }))

  //Build vehicle dot
  let vehicleTypeStyle = null;
  if (showDot) vehicleTypeStyle = new CircleStyle({
    radius: 26 * feature_scale * scale,
    fill: new Fill({
      color: vehicleColor
    })
  });

  //Build vehicle lable
  let textStyle = null;
  if (showLabel) textStyle = new TextStyle({
    fill: textColor,
    textAlign: 'left',
    scale: textScale * 1.6,
    offsetY: 3,
    offsetX: 35 * scale * feature_scale,
    text: labelText,
    backgroundFill: textBackgroundColor,
    padding: [3, 3, 0, 3]
  });

  //Build wehicle style array
  let styleArray = [
    new Style({
      zIndex: zIndex,
      image: vehicleTypeStyle,

    }),
    new Style({
      zIndex: zIndex,
      image: vehicleStyle,
      text: textStyle
    })
  ]

  //Build and add Ring
  if (ringColor) {
    //Build styles
    const ringStyle = new Icon(({
      anchor: [0.5, 0.5],
      anchorXUnits: 'fraction',
      anchorYUnits: 'fraction',
      src: "ring.svg",
      scale: scale * feature_scale,
      rotateWithView: true,
      color: ringColor
    }))

    styleArray.push(new Style({
      zIndex: zIndex,
      image: ringStyle
    }))
  }

  //Build and add Notification icon and text
  if (showNotificationIcon) {
    const vehicleWarningIcon = new Icon(({
      anchor: [1.2, 1.2],
      anchorXUnits: 'fraction',
      anchorYUnits: 'fraction',
      src: notificationIconSource,
      scale: scale * feature_scale * notificationIconScale * 1.6,
      rotateWithView: false,
      opacity: 1,
      //color: vehicleWarningIconColor
    }))

    let notificationStyle = null;
    if (isHovering && notificationText) notificationStyle = new TextStyle({
      fill: textColor,
      textAlign: 'right',
      scale: textScale * 1.2,
      offsetY: -24 * scale * feature_scale,
      offsetX: -52 * scale * feature_scale,
      text: notificationText + " >",
      backgroundFill: textBackgroundColor,
      padding: [3, 3, 0, 3]
    });

    styleArray.push(new Style({
      zIndex: zIndex,
      image: vehicleWarningIcon,
      text: notificationStyle
    }))
  }

  return styleArray;
}



const lastSearchText = {};
let inSearch = false;
let searchTimeout = null;

function updateSelectedVehicles() {
  if (!inSearch) {
    inSearch = true;
    document.getElementById("selectStatus").innerText = "Searching...";
    let searchBox = document.getElementById("searchBox").value;
    if (searchBox && searchBox !== "") {
      //console.log(indexSearch(searchBox));
      selectedVehicles = indexSearch(searchBox);
      //console.log(selectedVehicles);
    } else selectedVehicles = [];
    if (selectedVehicles.length === 1) document.getElementById("selectStatus").innerText = "1 vehicle selected";
    else if (selectedVehicles.length > 0) document.getElementById("selectStatus").innerText = selectedVehicles.length + " vehicles selected";
    else document.getElementById("selectStatus").innerText = "No vehicles selected";
    inSearch = false;
  } else {
    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(() => {
      updateSelectedVehicles();
    }, 1000);
  }
}


function updateFeature(vehicleRef, change) {
  let feature = vectorSource.getFeatureById(vehicleRef);
  let position = change.position;
  if (change.position) delete change.position;
  if (!feature) {
    if (position) {
      let location = geohash.decode(position);
      feature = new ol.Feature(new ol.geom.Point(ol.proj.fromLonLat([location.longitude, location.latitude])));
      feature.setId(vehicleRef);
      //feature.setStyle(vehicleTypeStyle);
      feature.setProperties(change);
      vectorSource.addFeature(feature);
      feature = vectorSource.getFeatureById(vehicleRef);
    }
  } else {
    feature.setProperties(change);
    if (position) {
      let location = geohash.decode(position);
      var newPosition = new ol.geom.Point(ol.proj.fromLonLat([location.longitude, location.latitude]));
      feature.setGeometry(newPosition);
      //window;
    }
  }

  let indexFeature = vectorSource.getFeatureById(vehicleRef);
  if (indexFeature) {
    let searchText = vehicleRef + " ";
    if (indexFeature.values_.vehicleIcon) searchText = searchText + indexFeature.values_.vehicleIcon + " ";
    if (indexFeature.values_.localVehicleId) searchText = searchText + indexFeature.values_.localVehicleId + " ";
    if (indexFeature.values_.sisId) searchText = searchText + indexFeature.values_.sisId + " ";
    if (indexFeature.values_.vixId) searchText = searchText + indexFeature.values_.vixId + " ";
    if (indexFeature.values_.nobinaId) searchText = searchText + indexFeature.values_.nobinaId + " ";
    if (indexFeature.values_.registrationNumber) searchText = searchText + indexFeature.values_.registrationNumber + " ";
    if (indexFeature.values_.contractAreaName) searchText = searchText + indexFeature.values_.contractAreaName + " ";
    if (indexFeature.values_.homeDepotName) searchText = searchText + indexFeature.values_.homeDepotName + " ";
    if (indexFeature.values_.operatorName) searchText = searchText + indexFeature.values_.operatorName + " ";
    if (indexFeature.values_.engineType) searchText = searchText + indexFeature.values_.engineType + " ";
    if (indexFeature.values_.gnssSource) searchText = searchText + indexFeature.values_.gnssSource + ".gnss ";
    if (indexFeature.values_.gnssStatus) searchText = searchText + indexFeature.values_.gnssStatus + " ";
    if (indexFeature.values_.externalDisplayCode) searchText = searchText + indexFeature.values_.externalDisplayCode + " ";
    if (indexFeature.values_.externalDisplayText) searchText = searchText + indexFeature.values_.externalDisplayText + " ";
    if (indexFeature.values_.externalDisplaySubText) searchText = searchText + indexFeature.values_.externalDisplaySubText + " ";
    if (indexFeature.values_.assignmentId) searchText = searchText + indexFeature.values_.assignmentId + ".block ";
    if (indexFeature.values_.evCharging === 1) searchText = searchText + "charging ";
    if (indexFeature.values_.isHidden === 1) searchText = searchText + "hidden ";
    //else if (indexFeature.values_.isHidden === 0) searchText = searchText + "online ";


    if (searchText !== lastSearchText[vehicleRef]) {
      const tokens = indexTokenizer(searchText)
      updateIndex(vehicleRef, tokens);

      updateSelectedVehicles();

    }
    lastSearchText[vehicleRef] = searchText;
  }
}


initMap();

let visibilityDisconnect = false;
let vehicleState = null;
let lookupList = null;
let lastSequenceNumber = null;

//Update connection stats
let lastStatTS = null;
let byteCounter = 0;
setInterval(() => {
  let now = new Date();
  if (lastStatTS) {
    const diff = (now - lastStatTS) / 1000
    const messageSpeed = ((byteCounter / diff) / 1024).toFixed(1)
    document.getElementById("posStats").innerText = messageSpeed + " kb/s";
    byteCounter = 0;
  }
  lastStatTS = now;
}, 5000);


socket.on("connect", () => {
  if (document.getElementById("message").innerText !== "") document.location.reload(true);
  else socket.emit("userId", userId);
});

socket.on("disconnect", (reason) => {
  //socket.connect();
  if (!visibilityDisconnect) {
    const messageBox = document.getElementById("message")
    let text = "Disconnected - " + reason;
    if (messageBox.innerText !== "") text = messageBox.innerText + "\n" + text;

    messageBox.innerText = text;
    document.getElementById("message_box").style.display = "flex";
  }
});

socket.on("connect_error", () => {

  socket.connect();
});

socket.on('vehicle', (vehicle) => {
  if (vehicle.key == selectedVehicleKey) {
    selectedVehicleData = vehicle.state;
  }
});

socket.on('reload', () => {
  document.location.reload(true);
});

socket.on('message', (message) => {
  const messageBox = document.getElementById("message")
  if (message) {
    messageBox.innerText = message;
    document.getElementById("message_box").style.display = "flex";
  } else {
    document.getElementById("message_box").style.display = "none";
    messageBox.innerText = "";
  }
});

socket.on('updates', (records) => {
  //console.log("Updates: ");
  const byteSize = str => new Blob([str]).size;

  byteCounter = byteCounter + byteSize(records);
  const updates = records.split("\n");

  const sequenceRecord = updates.shift();
  const sequenceNumber = sequenceRecord.split("§")[1];
  if (sequenceNumber == -1) {
    lastSequenceNumber = null;
    vehicleState = null;
    //lookupList = null;
  }
  //console.log(sequenceNumber);
  lastSequenceNumber++;
  if (lastSequenceNumber == 100) lastSequenceNumber = 0;
  if (sequenceNumber)
    if (!vehicleState) vehicleState = new Map();

  updates.forEach(record => {
    try {
      //if (record) {
      //console.log(record);
      const splitRecord = record.split("§");
      const command = splitRecord.shift();
      //try {
      const contentSplit = splitRecord.shift().split("^");
      //} catch (err) {
      //console.log(record);
      //}
      const id = contentSplit.shift();
      const value = contentSplit.shift();
      if (command == "F") addLookup(id, value);
      else if (command == "D") updateState(id, value);
      else if (command == "R") updateState(id, value);
    } catch {
      console.log("Error: " + record);
    }

  })

  function updateState(index, value) {
    const key = lookup(index);
    const record = decodeRecord(value);

    if (!vehicleState.has(key)) vehicleState.set(key, {});

    const vehicle = vehicleState.get(key);
    Object.entries(record).forEach(([fieldKey, value]) => {
      vehicle[fieldKey] = value;
    });

    updateFeature(key, record);
  }

  function decodeRecord(records) {
    const keyValuePairs = records.split("|");
    const result = {};
    keyValuePairs.forEach((record) => {
      const split = record.split("~");
      const field = lookup(split[0]);
      let value = split[1];
      if (!isNaN(value)) value = Number(value);
      else if (value == "null") value = null;
      else if (value == "true") value = true;
      else if (value == "false") value = false;
      result[field] = value;
    })
    return result
  }

  function lookup(index) {
    return lookupList.get(index);
  }

  function addLookup(index, key) {
    if (!lookupList) lookupList = new Map();
    lookupList.set(index, key);
  }
});


let disconnectTimer = null;
document.addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'visible') {
    visibilityDisconnect=false;
    if (disconnectTimer) {
      clearTimeout(disconnectTimer);
      disconnectTimer = null;
    }
    else document.location.reload(true);
  } else {
    clearTimeout(disconnectTimer);
    disconnectTimer = setTimeout(() => {
      visibilityDisconnect=true;
      socket.disconnect();
      disconnectTimer = null;
    }, 30000)

  }
});
