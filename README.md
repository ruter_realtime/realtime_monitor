# NOT Current


# Realtime Monitor
This application takes a vehicle states served on a kafka topics and displays the state on a map in realtime.





## Kafka topics
The states should be provided on 3 topics:
- **Realtime topic**, Realtime state of a vehicle. Data in this topic is synced to the map client in reatime and changes refresh the map. Use this for data that updates the visual rendering of the vehicles on the map or is used for filtering.
- **Detailed topic**, Detailed information about a vehicle. Data in this topic is available in the client for one vehicle when a vehicle is selected on the map. Data is updated in client as long as vehicle is selected. Map is NOT refreshed on updates to these data. Use this topic to provide detailed information about the selected vehicle.
- **History topic**, List of historic events of a vehicle. Data in this topic is available in the client for one vehicle when a vehicle is selected on the map. Data is updated in client as long as vehicle is selected. Map is NOT refreshed on updates to these data. Use this topic to display an event history for a selected vehicle.
The messages sould be published as JSON or AVRO. If AVRO is used a Schema repository containing the schema must be provided. The kafka consumer automatically detects correct format.

### Realtime topic format
The format is a flat (no nested properties) object. 
The following properties is a minimum to show vehicle on map:
- vehicleRef: The key of the vehicle, same as used in other topics.
- position: Position of vehicle encoded as a geoHash.
These fields control current map rendering:
- vehicleIcon: Icon to show for the vehicle. Current valid values: ['NONE','BUS','TRAM','FERRY','UNKNOWN']
- dotColor: Color of the dot behind the vehicle on the format #RRGGBBAA (all values in hex, AA is alfa. channel ak. transparrens in percent). #00000000 disables dot.
- ringColor: Color of the ring arround the dot behind the vehicle on the format #RRGGBBAA (all values in hex, AA is alfa. channel ak. transparrens in percent). #00000000 disables ring.
- notificationIcon: Icon attatched to vehicle to to notify user of vehicle status. Current valid values: ['NONE','STOP','WARNING-YELLOW','WARNING-ORANGE','WARNING-RED','COVERAGE-YELLOW','COVERAGE-ORANGE','COVERAGE-RED','INFO']
- notificationMessage: Info message related to vehicle. If not null an info icon is displayed on vehicle. 
- searchWords: String of frases used for filtering. Words/prases should be comma separated. Eg. line number, operator, status, notifications.
- shortLabel: Label added to vehicle when rendered at medium zoom-levels.
- longLabel: Label added to vehicle when rendered at high zoom-levels.
- gnssStatus: Current quality of position. Current valid values:['UNKNOWN','OFFLINE','NO_COVERAGE','OCCATIONAL',"NORMAL"];
In addition; any properties added to the record is replicated into the openLayers map feature and can be retrieved using `feature.get("<fieldName>")` inside the render function for the map. Remeber the limitation that the record must be flat. In other words NO NESTED PROPERTIES. 
Bandwith considerations: Positions is best encoded as Geohash, Timstamps as unix-epocs, booleans as 0/1. This saves bandwith.

## Detailed topic
Format is any object with nested (incuding arrays) properties of String, Number, Boolean or null. All values exept vehicleRef is optional.
The following properties is rendered in the vehicle info panel or as a detailed feature layer on map for a selected vehicle:
- vehicleRef: The key of the vehicle, same as used in other topics.
- operatorName: Name of operator
- vehicleStatus: Status of the vehicle. Current valid values:['OFFLINE','ON-DUTY','OFF-DUTY','UNKNOWN']
- engineType: Type of engine.
- sisId: SIS vehicle id.
- vin: Vehicle identification number.
- vixId: VIX identificator.
- nobinaId: Nobina identificator
- registrationNumber: Vehicle registration number
- localVehicleId: Local operator vehicle Id.
- homeDepotName: Name of home depot.
- contractAreaName: name of contract area.
- gnssSource: Source of current vehicle position. Current rendered values:['UNKNOWN','SIS','ITXPT','NOBINA','VIX','FALTCOM','PILOTFISH']
- externalDisplaySource: Source of current vehicle position. Current rendered values:['UNKNOWN','SIS','DPI']
- doorsOpen: Indicates if doors are open as 0/1.
- stopSignal: Indicates if stop signal is active as 0/1.
- charging: Indicates if vehicle is charging as 0/1.
- chargerConnected: Indicates if vehicle is connected to charger.
- stateOfCharge: Indicates current state of charge (SOC) on vehicle.
- externalTemperature: External temperature of vehicle.
- internalTemperature: Internal temperature of vehicle.
- numberOfPassengers: Number of passengers on board vehicle.
- nextQuayName: Name of next stop.
- nextQuayRef: Reference of next stop
- nextQuayLocation: location of next stop as a GeoHash.
- currentJourneyPatternPath: Line describing path of current journey as array of GeoHash.

## History topic
Format is an array of event objects describing historic vehicle events with a header. Used to renser a history panel for selected vehicle.
Header properties:
- vehicleRef: The key of the vehicle, same as used in other topics.
- lastUpdate: timestamp of last event (Unix-epoc).
- vehicleEvents: Array of vehicle event objects.
Vehicle events:
- timestamp: timestamp of event (Unix-epoc),
- category: Type of event. User defined
- description: Text describing event.
- notificationIcon: (Optional) Icon attatched to vehicle event. Current valid values: ['NONE','STOP','WARNING-YELLOW','WARNING-ORANGE','WARNING-RED','COVERAGE-YELLOW','COVERAGE-ORANGE','COVERAGE-RED','INFO']
- position: (Optional) Location of event as a GeoHash. Renders NotificationIcon at this position.
- geoJSON: (Optional) GeoJSON FeatureCollection object rendered when event is selected. Only geometry types "Point","LineString" and "Poligon" is currently supported.

Renderer currently handles the following styling of  GeoJSON objects, given in properties of GeoJSON feature. 
- fillColor: color to fill objects. Given as CSS color name, #RRGGBB or #RRGGBBAA
- strokeColor: color for lines. Given as CSS color name, #RRGGBB or #RRGGBBAA
- strokeType: Rendering of lines as ["NONE","LINE","DASH","DOT"];
- stokeWidth: Thiknes of lines
- pointRadius: Radius of circle to mark point
- label: Text label attached to feature
