'use strict';
const SERVICE_NAME = "RealtimeMonitor";
const serviceConfig = require("./lib/setup.js")(SERVICE_NAME);
const logger = serviceConfig.logger;
const os = require('os');
logger.info("ContainerId: " + os.hostname());

const KAFKA_BROKERS = process?.env?.KAFKA_BROKERS.split(",");
const VEHICLE_AGGREGATE_TOPIC = process.env.VEHICLE_AGGREGATE_TOPIC;
const PORT = process.env.PORT || 3000;
const NODE_ENV = process.env.NODE_ENV || null;

//Setup express
const express = require('express')
const app = express()

//Enable CORS during dev
if (NODE_ENV !== "production") {
    const cors = require("cors");
    app.use(cors());
}

//Share client
app.use(express.static('public'));
let io=null;



//Setup Socket.IO websocket server


const Consumer = require("async-kafka").Consumer;
const consumer = new Consumer({
    kafkaBrokers: KAFKA_BROKERS, //Array of kafka brokers
    schemaRegistryUrls: null,             //Array of schema registry URLs
    groupId: SERVICE_NAME + "d-" + os.hostname(),                       //If not specified a random GUID wil be used and commits will not function
    clientId: SERVICE_NAME + "1",                         //If not specified a random GUID wil be used
    maxQueueSize: 2000,                                 //Maximum number of messages to keep in async queue
    statsIntervalSec: 20,                                //Interval between stats calculation
    maxFetchSize: 1000,                                  //Maximum number of messages to fetch in a batch. Processing of batch is syncrounous and influences flow.
    commitIntervalMs: 1000,                              //Interval to commit offsets if topic has 'autoCommit' set
    maxEventLoopBlockMs: 10,                            //Maximum time in ms that batch processing will be allowed to block event loop.
    maxWaitMs: 10,                                      //Maximum time to wait to fill a batch of messages. Low number keeps latency low at the cost of IO
    logStats: false,                                      //Log statistics to provided logger
    automaticGracefulShudown: false
}, logger);

const State = require("@zacken/async_state_manager");
let vehicleMasterState = null;

const VehicleStateServer = require("./vehicleStateServer");
let state=null; 

async function main() {

    logger.info('Starting Service: ' + serviceConfig.serviceName);

    state = new VehicleStateServer(io, app, logger, [
        "vehicleRef",
        "position",
        "gnssStatus",
        "isHidden",
        "gnssSource",
        "doorsOpen",
        "stopSignal",
        "externalDisplayCode",
        "externalDisplayText",
        "externalDisplaySubText",
        "externalTemperature",
        "cabinTemperature",
        "evStateOfCharge",
        "evCharging",
        "blockRef",
        "assignmentId",
        "assignmentSource",
        "journeyRef",
        "journeyId",
        "journeyName",
        "journeySource",
        "isDeadRun",
        "externalDisplaySource",
        "vehicleIcon",
        "livery",
        "dotColor",
        "operatorName",
        "localVehicleId",
        "sisId",
        "nobinaId",
        "vixId",
        "vin",
        "registrationNumber",
        "engineType",
        "contractAreaName",
        "homeDepotName",
        "operatorCode",
        "hasLowFloorAccess",
        "operatorRef",
        "babyCarriageCapacity",
        "doorConfiguration",
        "numberOfDoors",
        "totalPassengerCapacity",
        "seatedPassengerCapacity",
        "tipupSeatPassengerCapacity",
        "wheelcairCapacity",
        "length",
        "height"]);

    await consumer.connect();

    vehicleMasterState = new State({
        stateStoreType: "kafka",
        changeHandler: async (key, record) => {
            vehicleMasterHandler(key, record);
        },
        persistState: false,
        processRecordsAfterLoaded: true,
        automaticGracefulShudown: false,
        kafka: {
            consumer: consumer,
            topic: VEHICLE_AGGREGATE_TOPIC
        }
    });

    async function vehicleMasterHandler(key, message) {
        if (message) {
            await state.updateState("PUSH", key, message.push);
            await state.updateState("PULL", key, message.pull);
        } else {
            ////!!!!  implement deletion of state
        }
    }
    state.pause("Server is starting");

    await vehicleMasterState.connect();

    state.resume();
    logger.info('Service is started');
    //io.emit('reload', true);
}



process.on("unhandledRejection", function (error) {
    logger.error("Caugth unhandled Rejection from process");
    shutdownAndExit(error);
});

process.on("uncaughtException", function (error) {
    logger.error("Caugth unhandled Exception from process");
    shutdownAndExit(error);
});

process.on('SIGINT', function () {
    logger.info("Recieved SIGINT from process");
    shutdownAndExit();
});

async function shutdownAndExit(error) {
    let exitCode = 0;
    if (error) {
        logger.error(error);
        exitCode = 1;
    }
    try {
        state.pause("Server is shutting down");
        await vehicleMasterState.disconnect();
        //logger.info("Closing Kafka");
        await consumer.disconnect();
    } catch (err) {
        logger.error("Gracefull shutdown failed");
        logger.error(err);
        exitCode = 1;
    }
    logger.info("Gracefull shutdown complete");
    logger.info("exitCode: " + exitCode);
    process.exit(exitCode);
}

const server = app.listen(PORT, () => {
    logger.info(`Webservice listening at port ${PORT}`);
    io = require('socket.io')(server, {
        serveClient: true
    });
    main();
});






