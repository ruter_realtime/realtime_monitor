import {io} from 'socket.io-client';
const ENDPOINT = "http://localhost:3000";

class SocketConnection {
    constructor(endPoint){
        this.endPoint=endPoint ||ENDPOINT;
        this.socket=null;

        this.socket = io.connect(this.endPoint,{
            transports: ['websocket'],
            autoConnect: false,
            forceNew: true,
          }
        );

        this.socket.on("error",(error)=>{
            console.log(error);
        });
        this.socket.on("connect",()=>{
            console.log("connected");
        });
        this.socket.on("disconnect",()=>{
            console.log("disconnect");
        });



    }

    connect(){
        console.log("connecting");
        this.socket.open()
        this.socket.on('connect', () => {
            console.log("connected");
            
          })
        //console.log(this.socket);

    }

    disconnect(){

    }
}




export { SocketConnection };