import React from "react"


class SocketConnection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connectionStatus: "UNKNOWN",
      mounted: false
    };


  }



  componentDidMount() {
    //if(!this.socket) this.connectSocket();
  }

  componentWillUnmount() {

  }

  render() {
    return (
      <div>
        <h2>Status: {this.state.connectionStatus}</h2>
        <h2>Error: {this.state.connectionError}</h2>
      </div>
    );
  }
}


export default SocketConnection;