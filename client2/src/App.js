import React from 'react';
import './App.css';

import { SocketContext, socket } from './context/socket';
import SocketConnection from "./SocketConnection";


const App = () => {


    return (
        <div>
            <SocketContext.Provider value={socket}>
                <SocketConnection />
            </SocketContext.Provider>
        </div>
    );
}

export default App;