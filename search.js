const index = {};
const ids = new Map();

function updateIndex(id, keywords) {
    deleteDocumentIndex(id);
    ids.set(id, keywords);
    keywords.forEach(keyword => {
        const trimmedKeyword=keyword.trim().toLowerCase();
        if (!index[trimmedKeyword]) index[trimmedKeyword] = new Map();
        index[trimmedKeyword].set(id, id);
    })
}

function deleteDocumentIndex(id) {
    const keywords = ids.get(id);
    if (keywords) {
        keywords.forEach(keyword => {
            index[keyword.trim().toLowerCase()].delete(id);
            if (!index[keyword.trim().toLowerCase()].size) delete index[keyword.trim().toLowerCase()];
        });
        ids.delete(id);
    }
}

function indexTokenizer(text) {
    const tokens = [];
    const words = text.toLowerCase().split(" ");
    words.forEach(word => {
        const trimmedWord = word.trim();
        if (trimmedWord && trimmedWord !== "") {
            if (trimmedWord.length > 2) {
                for (let n = 2; n < trimmedWord.length; n++) {
                    tokens.push(trimmedWord.substr(0, n + 1));
                }
            } else {
                tokens.push(trimmedWord);
            }
        }
    })
    return tokens;
}

function indexSearch(searchPhrase) {
    let result = [];
    const andParts = searchPhrase.split(" ");
    let first = true;
    andParts.forEach(part => {
        const trimmedPart = part.trim();
        if (trimmedPart && trimmedPart !== "") {
            let ids = [];
            if (trimmedPart.indexOf("|")!==null) {
                const orParts = trimmedPart.split("|");
                orParts.forEach(orPart => {
                    const orPartIds = index[orPart.trim().toLowerCase()];
                    if (orPartIds)
                        orPartIds.forEach(id => {
                            if (!ids.includes(id)) ids.push(id);
                        })
                })
            }
            else ids = index[trimmedPart.toLowerCase()];
            if (ids) {
                const newResult = [];
                ids.forEach(id => {
                    if (first) newResult.push(id);
                    else {
                        if (result.includes(id)) newResult.push(id);
                    }
                })
                result = newResult;
            } else result = [];
            first = false;
        }
    })

    return result;
}


updateIndex("LKLKJLKJ", ["hei", "på", "deg", "xyz"]);
updateIndex("2", ["låven", "på", "xyz"]);
updateIndex("LKLKJLKJ", indexTokenizer("låve på 123 lagerhall"));
updateIndex("2", indexTokenizer("henrik lager låvedør"));

//console.log(search(" lagerhl "));
console.log(indexSearch("123|låvedør henrik|||"));