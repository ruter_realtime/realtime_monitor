'use strict';

function serviceSetup(serviceName) {

    //Setup winston logger
    const winston = require("winston");
    const logLevel = process.env.LOG_LEVEL || 'info';
    var prefix = "";
    if (serviceName) prefix = serviceName + ": ";
    const logger = winston.createLogger({
        level: logLevel,

        format: winston.format.combine(
            winston.format.colorize(),
            //winston.format.simple(),
            winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            winston.format.label({ label: serviceName }),

            //winston.format.prettyPrint(),
            winston.format.printf(({ level, message, label, timestamp }) => `[${label}] ${timestamp} ${level}: ${message}`),
            winston.format.errors({ stack: true }),
        ),
        transports: [
            new winston.transports.Console({
                //label: moduleName
            })
        ]
    });

    //Parse .env file if any
    const dotEnvResult = require('dotenv').config()
    if (dotEnvResult.error) {
        logger.info("No .env found. Using standard enviroment.");
    } else logger.info("Loaded enviroment from .env: " + JSON.stringify(dotEnvResult.parsed));

    return {
        logger,
        serviceName
    }
}

module.exports = serviceSetup;