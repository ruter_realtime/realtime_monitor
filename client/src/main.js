import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import * as Projection from 'ol/proj'

const Circle = ol.geom.Circle;
const Circle = ol.geom.Circle;
const Circle = ol.geom.Circle;
const RegularShape = ol.style.RegularShape;
const Feature = ol.Feature;
const Point = ol.geom.Point;
const VectorSource = ol.source.Vector;
const VectorLayer = ol.layer.Vector;
const Style = ol.style.Style;
const CircleStyle = ol.style.Circle;
const PolygonStyle = ol.style.Polygon;
const TextStyle = ol.style.Text;
const Fill = ol.style.Fill;
const Stroke = ol.style.Stroke;
const Icon = ol.style.Icon;
const Select = ol.interaction.Select;
const Color = ol.color;
const LineString = ol.geom.LineString;
const Attribution = ol.control.Attribution;
const DefaultControls = ol.control.defaults;


//Setup map

/*
const arcgisSatLayer = new TileLayer({
  source: new XYZ({

    attributions: 'Orthophoto © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a> ',
    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Imagery/MapServer/tile/{z}/{y}/{x}'
  }),
  preload: 12,
  //minZoom: 12,
  //maxZoom: 19,
})*/

const OSMLayer = new TileLayer({
  source: new XYZ({
    url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
  })
})


function centerMap(map, pos) {
  //let loc = ol.proj.fromLonLat(pos);
  let view = map.getView();
  view.setCenter(pos);
}


const map = new Map({
  target: 'map',
  layers: [OSMLayer],
  //controls: DefaultControls({ attribution: false }).extend([attribution]),
  view: new View({
    zoom: 13,
    maxZoom: 19,
    enableRotation: false
  })
});

centerMap(map, Projection.fromLonLat([10.732747, 59.910946]))



const socket = io.connect();
let lastSequenceNumber = null;
let fieldLookup = {};

socket.on('lookup', (lookup) => {
  
  fieldLookup = lookup;
  console.log("Lookup: ");
  console.log(lookup);

});

socket.on('updates', (records) => {
  console.log("Updates: ");
  
  const updates=records.split("^");
  
  updates.forEach(record=>{
    console.log(record);
    //const update = decode(record, fieldLookup);
    //if(!update) socket.emit("restart", true);
  })
});


function decode(record, fieldLookup) {
  //console.log(record);
  const result = { record: {} };

  if (record) {
    let missingFieldLookup = false;
    const recordParts = record.split("|");

    result.id = fieldLookup[recordParts.shift().toString()];
    if (result.id === undefined) {
      missingFieldLookup = true;
      result.id = "missing"
    }

    while (recordParts.length > 0) {

      let key = fieldLookup[recordParts.shift().toString()];
      if (key === undefined) {
        missingFieldLookup = true;
        key = "missing"
      }
      const value = recordParts.shift();
      result.record[key] = value;
    }

    //console.log(result)
    return result;
  } else return null;
}