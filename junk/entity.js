"use strict";

//const EventEmitter = require('events');
//const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");

class Entity {
    constructor(reference, properties, entityCollection) {
        //super();
        this.initialized = false;
        if (reference) {
            this.initialize(reference, properties, entityCollection)
        } else this.initialized = false;
    }

    initialize(reference, properties, entityCollection) {
        if (!this.initialized) {
            this.key = reference;
            this.initialized = true;
            this.state = "UNKNOWN";
            this.hasFinalState = false;
            this.set(properties);
            if (entityCollection) {
                entityCollection.set(reference, this);
                this.entityCollection = entityCollection;
            }

        } else throw ("Trying to initialize an initialized entity: " + this.key);
    }

    clear() {
        _throwErrorIfNotInitialized(this, "Trying to clear an un-initialized entity");
        this.state = "UNKNOWN";
        this.hasFinalState = false;
        this.set();
    }

    delete() {
        _throwErrorIfNotInitialized(this, "Trying to delete an un-initialized entity");
        if (this.entityCollection) {
            this.entityCollection.delete(this.key);
            this.entityCollection = null;
        }
        this.key = null;
        this.properties = null;
        this.initialized = false;
        this.state=null;
        this.hasFinalState=null;
    }

    set(properties) {
        _throwErrorIfNotInitialized(this, "Trying to set an un-initialized entity");
        if(properties?.key) throw ("Entity can not be set on initialized entity");
        if(properties?.state) throw ("State can only be changed with setState.");
        this.properties = properties || {};
        this.properties.key=this.key;
        this.state={value:this.state,isFinal:this.hasFinalState};
    }

    get() {
        _throwErrorIfNotInitialized(this, "Trying to get an un-initialized entity");
        return this.properties;
    }

    merge(properties, options) {
        _throwErrorIfNotInitialized(this, "Trying to merge to an un-initialized entity");
        if(properties?.key) throw ("Entity can not be set on initialized entity");
        if(properties?.state) throw ("State can only be changed with setState.");
        let mergeOptions = (destinationArray, sourceArray, options) => sourceArray;
        if (properties) {
            if (options?.arrayMerge)
                if (options.arrayMerge == "replace") mergeOptions.arrayMerge = (destinationArray, sourceArray, options) => sourceArray;
                else if (options.arrayMerge == "add") mergeOptions.arrayMerge = (destinationArray, sourceArray, options) => destinationArray.concat(sourceArray)
                else if (options.arrayMerge == "unique") mergeOptions.arrayMerge = (destinationArray, sourceArray, options) => {
                    let result = [];
                    let totalArray = destinationArray.concat(sourceArray);
                    totalArray.forEach((value) => {
                        if (!result.includes(value)) result.push(value);
                    });
                    return result;
                }
                else throw ("Trying to use unknown array merge type in entity merge (valid: overwrite/add/unique): " + options.arrayMerge)

            const newProperties=deepmerge(this.properties, properties, mergeOptions);
            delete newProperties.key;
            delete newProperties.state;
            this.set(newProperties);
        } else throw ("Trying to merge type nothing to entity")
    }

    moveToEntityCollection(newEntityCollection) {
        _throwErrorIfNotInitialized(this, "Trying to move an un-initialized entity to a new collection");
        if (this.entityCollection) {
            this.entityCollection.delete(this.key);
        }
        if (newEntityCollection) {
            newEntityCollection.set(this.key, this);
            this.entityCollection = newEntityCollection;
        } else throw ("Trying to set entity to a non-existing collection")
    }

    removeFromEntityCollection() {
        _throwErrorIfNotInitialized(this, "Trying to remove an un-initialized entity from a collection");
        if (this.entityCollection) {
            this.entityCollection.delete(this.key);
            this.entityCollection = null;
        } else throw ("Trying to remove entity from a non-existing collection")
    }

    setState(state, isFinal) {
        _throwErrorIfNotInitialized(this, "Trying to set status on an un-initialized entity");
        const hasFinalState = isFinal || false;
        if (!this.hasFinalState) {
            this.state = state;
            this.hasFinalState = hasFinalState;
            this.properties.state={state:state,isFinal:hasFinalState};
        } else throw ("Trying to set status on entity with final status")
    }
}

function _throwErrorIfNotInitialized(context, message) {
    if (!context.initialized) throw (message);
}

module.exports = Entity;