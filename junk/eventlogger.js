const clone = require('clone');

async function main() {
    var runCleaner = false;

    const StateManager = require("kafka-state-manager").StateManager; // Kafka pakage

    const stateManager = new StateManager(
        {
            groupId: "realtime_monitor1",
            clientId: "realtime_monitor1",
            brokerList: ["10.10.0.13:9092"],
            schemaRegistryUrl: "http://10.10.0.13:8081",
            queueSize: 2000, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
            compression: "none",
            statsLogIntervalSec: 10,
            stateLogIntervalSec: 20,
            memoryLogIntervalSec: 20,
            states: [
                { stateName: "vehicle", changeHandler: null },
                { stateName: "actualblock", cleaner: transactionalEntityCleaner },
                { stateName: "actualjourney", cleaner: transactionalEntityCleaner },
                { stateName: "actualcall", cleaner: transactionalEntityCleaner, changeHandler: null },
            ]
        }
    );

    async function transactionalEntityCleaner(stateName, stateKey, stateRecord, state) {
        let result = true;
        if (runCleaner) {
            const now = new Date();
            let then;
            if (stateRecord?.updateTimestamp) then = new Date(stateRecord.updateTimestamp);
            else then = new Date(stateRecord.timestamp);
            const diff = (now - then);
            if (diff > 60000) {
                if ((stateName == "actualblock") && (stateRecord.status == "DONE"))
                    result = false;
                else if (stateName == "actualjourney") {
                    if (stateRecord.actualBlockRef) {
                        const actualBlockExists = state.get("actualblock").has(stateRecord.actualBlockRef);
                        if (!actualBlockExists)
                            result = false;
                    }
                }
                else if (stateName == "actualcall") {
                    if (stateRecord.actualJourneyRef) {
                        const actualJourneyExists = state.get("actualjourney").has(stateRecord.actualJourneyRef);
                        if (!actualJourneyExists)
                            result = false;
                    }
                }
            }

            if (diff > (60000 * 60 * 24))
                result = false;

        }
        return result;
    }

    await stateManager.connect();
    await stateManager.subscribe([
        /* {
             topic: "STAGE.entity.actualblock.key",
             mapper: entityMapper,
             keepOffsetOnReassign: true,
             startFromLatest: false,    // Starts from latest message on connect
             messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
             maxAgeSec: 3600 * 12
         },
         {
             topic: "STAGE.entity.actualjourney.key",
             mapper: entityMapper,
             keepOffsetOnReassign: true,
             startFromLatest: false,    // Starts from latest message on connect
             messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
             maxAgeSec: 3600 * 12
         },*/
        {
            topic: "STAGE.entity.actualcall.key.v2",
            mapper: entityMapper,
            keepOffsetOnReassign: true,
            startFromLatest: false,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
            maxAgeSec: 3600 * 1
        }
    ]);

    async function entityMapper(states, message) {
        let result = [];
        if (message?.value?.EntityPartition) {
            const record = clone(message.value.EntityPartition);
            record.Timestamp = message.value.Timestamp;
            record.Key = message.value.EntityPartition.Key;

            const stateName = message.value.Name;

            let state = {
                stateName: stateName,
                stateKey: message.value.EntityPartition.Key,
                stateRecord: record,
                merge: true
            }
            result.push(state);
        }
        return result;
    }


    await stateManager.subscribe([
        {
            topic: "STAGE.entity.actualblock.status",
            mapper: actualBlockStatusMapper,
            keepOffsetOnReassign: true,
            startFromLatest: true,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualjourney.status",
            mapper: statusMapper,
            keepOffsetOnReassign: true,
            startFromLatest: true,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualcall.status",
            mapper: actualCallStatusMapper,
            keepOffsetOnReassign: true,
            startFromLatest: true,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        {
            topic: "STAGE.entity.actualcall.estimatedtime",
            mapper: actualCallETAMapper,
            keepOffsetOnReassign: true,
            startFromLatest: true,    // Starts from latest message on connect
            messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
        },
        /* {
             topic: "STAGE.entity.pass.key",
             mapper: statusMapper,
             keepOffsetOnReassign: true,
             startFromLatest: true,    // Starts from latest message on connect
             messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
             maxAgeSec: 3600 * 12
         }*/
    ]);

    async function actualBlockStatusMapper(states, message) {
        let result = [];
        if (message?.value?.EntityPartition) {
            const record = clone(message.value.EntityPartition);
            record.Timestamp = message.value.Timestamp;
            record.Key = message.value.EntityPartition.Key;

            const stateName = message.value.Name;

            let state = {
                stateName: stateName,
                stateKey: message.value.EntityPartition.Key,
                stateRecord: record,
                merge: true
            }
            result.push(state);
        }
        return result;
    }

    async function actualCallStatusMapper(states, message) {
        let result = [];
        if (message?.value?.EntityPartition) {
            const actualCall = states.get('actualcall').get(message.value.EntityPartition.Key);
            if (actualCall) {
                const record = {
                    LastLogType: "ACTUAL_CALL_STATUS",
                    ActualCall: {
                        Status: message.value.EntityPartition.Status,
                        DoneStatus: message.value.EntityPartition.DoneStatus,
                        ArrivalDelay: message.value.EntityPartition.ArrivalDelay,
                        DepartureDelay: message.value.EntityPartition.DepartureDelay
                    }
                }
                record.Timestamp = message.value.Timestamp;
                record.Key = actualCall.VehicleRef;

                const stateName = message.value.Name;

                let state = {
                    stateName: "vehicle",
                    stateKey: actualCall.VehicleRef,
                    stateRecord: record,
                    merge: true
                }
                result.push(state);
            }
        }
        return result;
    }

    async function actualCallETAMapper(states, message) {
        let result = [];
        if (message?.value?.EntityPartition) {
            const actualCall = states.get('actualcall').get(message.value.EntityPartition.Key);
            if (actualCall) {
                const record = {
                    LastLogType: "ACTUAL_CALL_ETA",
                    ActualCall: {
                        EstimatedTimeOfArrival: message.value.EntityPartition.EstimatedTimeOfArrival,
                        EstimatedTimeOfDeparture: message.value.EntityPartition.EstimatedTimeOfDeparture,
                        ArrivalDelay: message.value.EntityPartition.ArrivalDelay,
                        DepartureDelay: message.value.EntityPartition.DepartureDelay
                    }
                }
                record.Timestamp = message.value.Timestamp;
                record.Key = actualCall.VehicleRef;

                const stateName = message.value.Name;

                let state = {
                    stateName: "vehicle",
                    stateKey: actualCall.VehicleRef,
                    stateRecord: record,
                    merge: true
                }
                result.push(state);
            }
        }
        return result;
    }

    async function statusMapper(states, message) {
        let result = [];
        if (message?.value?.EntityPartition) {
            const record = clone(message.value.EntityPartition);
            record.Timestamp = message.value.Timestamp;
            record.Key = message.value.EntityPartition.Key;

            const stateName = message.value.Name;

            let state = {
                stateName: stateName,
                stateKey: message.value.EntityPartition.Key,
                stateRecord: record,
                merge: true
            }
            result.push(state);
        }
        return result;
    }

}

main();