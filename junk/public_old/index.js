

const Circle = ol.geom.Circle;
const RegularShape = ol.style.RegularShape;
const Feature = ol.Feature;
const Point = ol.geom.Point;
const VectorSource = ol.source.Vector;
const VectorLayer = ol.layer.Vector;
const Style = ol.style.Style;
const CircleStyle = ol.style.Circle;
const PolygonStyle = ol.style.Polygon;
const TextStyle = ol.style.Text;
const Fill = ol.style.Fill;
const Stroke = ol.style.Stroke;
const Icon = ol.style.Icon;
const Select = ol.interaction.Select;
const Color = ol.color;
const LineString = ol.geom.LineString;
const Attribution = ol.control.Attribution;
const DefaultControls = ol.control.defaults;

var map;
var vectorSource;
var journeyPatternSource;
var selectedVehicle;
var isTouchDevice = (navigator.maxTouchPoints > 1);

var vehicleFilter = "ALL";
var statusFilter = "ALL";

function setFilter(type, value) {
  if (type == "vehicle") vehicleFilter = value;
  else if (type == "status") statusFilter = value;
  vectorSource.changed();
}



function colorWithAlpha(color, alpha) {
  const [r, g, b] = Array.from(Color.asArray(color));
  return Color.asString([r, g, b, alpha]);
}



function initMap() {

  var attribution = new Attribution({
    collapsible: true
  });

  var computeQuadKey = function (x, y, z) {
    var quadKeyDigits = [];
    for (var i = z; i > 0; i--) {
      var digit = 0;
      var mask = 1 << (i - 1);

      if ((x & mask) != 0)
        digit++;
      if ((y & mask) != 0)
        digit = digit + 2;

      quadKeyDigits.push(digit);
    }
    return quadKeyDigits.join('');
  };

  var bingTrafficSource = new ol.source.XYZ({
    //minZoom: 12,
    //maxZoom: 14,
    tileUrlFunction(tileCoord, pixelRatio, projection) {
      var z = tileCoord[0];
      var x = tileCoord[1];
      var y = -tileCoord[2] - 1;
      return "https://t0.tiles.virtualearth.net/tiles/t" + computeQuadKey(x, y, z) + ".png";
    },
    attributions: 'Traffic layer © <a href="https://www.bing.com/maps">Bing</a> '
  });




  var bingTrafficLayer = new ol.layer.Tile({
    source: bingTrafficSource,
    maxResolution: 200,
    preload: 12
  });

  var arcgisSatLayer = new ol.layer.Tile({
    source: new ol.source.XYZ({

      attributions: 'Orthophoto © <a href="https://services.arcgisonline.com/ArcGIS/' +
        'rest/services/World_Topo_Map/MapServer">ArcGIS</a> ',
      url: 'https://server.arcgisonline.com/ArcGIS/rest/services/' +
        'World_Imagery/MapServer/tile/{z}/{y}/{x}'
    }),
    preload: 12,
    //minZoom: 12,
    //maxZoom: 19,
  })


  var _url = "http://opencache.statkart.no/gatekeeper/gk/gk.open?";

  //Start: source
  var sourceWMSC = new ol.source.TileWMS({
    url: _url,
    params: {
      LAYERS: 'norges_grunnkart',
      VERSION: '1.1.1'
    }
  });
  //End: source

  //Start: layer
  var tileLayerWMSC = new ol.layer.Tile({
    title: "Norges grunnkart",
    source: sourceWMSC,
    //preload: 12
  });


  var tileLayerOSM = new ol.layer.Tile({
    preload: 12,
    source: new ol.source.OSM()
  });

  const urlParams = new URLSearchParams(window.location.search);
  let showLayers = [];
  if (urlParams.get("satelitt") == "true") showLayers.push(arcgisSatLayer);
  else showLayers.push(tileLayerOSM);
  if (urlParams.get("trafikk") == "true") {
    showLayers.push(bingTrafficLayer);
    setInterval(() => {
      console.log("Refreshing traffic layer");
      let source = bingTrafficLayer.getSource();
      source.tileCache.expireCache({});
      source.tileCache.clear();
      source.refresh({ force: true });
    }, 180000);
  }

  function centerMap(pos) {
    //let loc = ol.proj.fromLonLat(pos);
    let view = map.getView();
    view.setCenter(pos);
  }


  map = new ol.Map({
    target: 'map',
    layers: showLayers,
    controls: DefaultControls({ attribution: false }).extend([attribution]),
    view: new ol.View({
      //center: ol.proj.fromLonLat([10.732747, 59.910946]),
      zoom: 13,
      //minZoom: 11,
      maxZoom: 19,
      enableRotation: false
    })
  });

  centerMap(ol.proj.fromLonLat([10.732747, 59.910946]))

  vectorSource = new VectorSource({
    features: []

  });

  var vectorLayer = new VectorLayer({
    source: vectorSource,
    style: markerStyleFunction
  });

  map.addLayer(vectorLayer);

  journeyPatternSource = new VectorSource({
    features: []

  });

  var journeyPatternLayer = new VectorLayer({
    source: journeyPatternSource,
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({ color: 'royalblue', width: 4 })
    })
  });

  map.addLayer(journeyPatternLayer);


  var selectSingleClick = new Select({
    style: markerStyleFunction
  });
  map.addInteraction(selectSingleClick);
  selectSingleClick.on('select', function (e) {

    if (e.selected[0]) {
      selectedVehicle = e.selected[0].id_;
      console.log("Selected vehicle: " + selectedVehicle);
    }
    else selectedVehicle = null;
  });


}

let lastMove = 0;

function markerStyleFunction(feature, resolution) {

  var vehicleRef = feature.id_;

  var hasSelectedVehicle = false;
  var isSelectedVehicle = false;
  if (selectedVehicle) {
    hasSelectedVehicle = true;
    if (selectedVehicle == vehicleRef) isSelectedVehicle = true;
  }

  /**/

  var vehicleType = feature.get("vehicleType");
  var vehicleStatus = feature.get("vehicleStatus");
  var telemetryType = feature.get("telemetryType");
  var publicCode = feature.get("publicCode");
  var displayText = feature.get("displayText");
  var viaText = feature.get("viaText");
  var isStopped = feature.get("isStopped");
  var isParked = feature.get("isParked");
  var isStale = feature.get("isStale");
  var hasDoorsOpen = feature.get("hasDoorsOpen");
  var hasStopSignal = feature.get("hasStopSignal");
  var nextStopLabel = feature.get("nextStopLabel");
  var nextStopLocation = feature.get("nextStopLocation");
  var nextStopETATimestamp = feature.get("nextStopETATimestamp");
  var lastSignonBlockID = feature.get("lastSignonBlockID");
  var lastSignonJourneyID = feature.get("lastSignonJourneyID");
  var lastSignonTS = feature.get("lastSignonTS");

  textScale = 1;
  //color = 'red';
  feature_scale = 1;
  textColor = new Fill({
    color: 'black'
  });
  backgroundColor = new Fill({
    color: 'rgba(255, 255, 255, 0.5)'
  });

  //if (isParked) vehicleStatus = "PARKED";

  if (vehicleStatus == "OFFLINE") color = colorWithAlpha('white', 0.6);
  else if (vehicleStatus == "PARKED") color = colorWithAlpha('lightgray', 0.6);
  else if (vehicleStatus == "NO_SIGNON") color = colorWithAlpha('gray', 0.6);
  else if (vehicleStatus == "SIGNON_FAILED") color = colorWithAlpha('red', 0.6);
  else if (vehicleStatus == "PROGRESS_WITHOUT_SIGNON") color = colorWithAlpha('yellow', 0.6);
  else if (vehicleStatus == "NO_PROGRESS") color = colorWithAlpha('orange', 0.6);
  else if (vehicleStatus == "SIGNON") {
    var tempColor = "blue";
    if (displayText == "Ikke i trafikk") tempColor = "blue";
    if (telemetryType == "ITXPT") color = colorWithAlpha(tempColor, 0.9);
    else if (telemetryType == "HYBRID") color = colorWithAlpha(tempColor, 0.7);
    else if (telemetryType == "VIX") color = colorWithAlpha(tempColor, 0.5);
    else if (telemetryType == "SIS") color = colorWithAlpha(tempColor, 0.3);
  }
  else color = colorWithAlpha('purple', 0.3);

  //if (telemetryType == "VIX") color = colorWithAlpha("green", 1);

  scale = 0.7;

  var labelText;
  if (publicCode) labelText = publicCode + " " + displayText;
  else labelText = displayText;
  if (viaText) labelText = labelText + '\n' + viaText;


  var showLabel = true;

  var vehicleIconOccupacy = 1;
  if (hasSelectedVehicle) {
    document.getElementById("overlay").style.display = "block";

    if (!isSelectedVehicle) {
      color = 'gray';
      feature_scale = 0.7;
      showLabel = false;
      vehicleIconOccupacy = 0.5;
    } else {



      /*let pos = feature.getGeometry().getCoordinates();
      let diff = Math.abs((pos[0] + pos[1]) - lastMove)
      if (diff > 150 * resolution) {

        let view = map.getView();
        view.animate({
          center: pos,
          duration: 2000
        });
        //view.setCenter(pos);
        lastMove = (pos[0] + pos[1]);
      }
      //centerMap(pos);*/

      feature_scale = 1;

      var typeElement = document.getElementById("type");
      typeElement.style.color = "blue";
      typeElement.innerText = telemetryType;
      document.getElementById("id").innerText = vehicleRef;
      var statusText = ""

      document.getElementById("display").innerText = labelText;

      //if ((status == "UNKNOWN") || (status == "OK") || (status === undefined)) statusText = ""
      var statusText = "";
      if (nextStopLabel) {
        statusText = nextStopLabel;
        if (hasDoorsOpen) statusText = statusText + " (DOORS OPEN)";
        else if (hasStopSignal) statusText = statusText + " (STOPPING)";

      }
      document.getElementById("nextStop").innerText = statusText;
      document.getElementById("nextStop").style.color = "gray";

      document.getElementById("status").innerText = "Status: " + vehicleStatus;
      document.getElementById("status").style.color = "gray";

      var signonText = "";

      if (lastSignonTS) {
        if (lastSignonJourneyID.length > 8) signonText = lastSignonJourneyID;
        else signonText = lastSignonBlockID + "/" + lastSignonJourneyID + " (TS: " + lastSignonTS + ")";
      }

      document.getElementById("signon").innerText = "Last signon: " + signonText;
      document.getElementById("signon").style.color = "gray";

      if (nextStopLocation) {
        var location = geohash.decode(nextStopLocation);

        var stopPlace = ol.proj.fromLonLat([location.longitude, location.latitude]);
        var vehiclePoint = feature.getGeometry().getCoordinates();
        var nextStopLine = [stopPlace, vehiclePoint];

        var featureLine = journeyPatternSource.getFeatureById("nextStopLine");
        if (featureLine) {
          featureLine.setGeometry(new LineString(nextStopLine));
        }
        else {
          featureLine = new ol.Feature({
            geometry: new LineString(nextStopLine)
          });

          featureLine.setId("nextStopLine");
          journeyPatternSource.addFeature(featureLine);
        }

      }
    }
  } else {
    document.getElementById("overlay").style.display = "none";;
    journeyPatternSource.clear();
  }


  var vehicleIcon = "bus.svg";
  var vehicleIconScale = 1;
  if (vehicleType == "TRAM") {
    vehicleIcon = "tram.svg";
    vehicleIconScale = 1.3;
  }
  else if (vehicleType == "FERRY") {
    vehicleIcon = "ferry.svg";
    vehicleIconScale = 1.2;
  }
  var showIcon = true;
  var hasError = false;
  var status_color = "black";
  if (hasDoorsOpen) {
    hasError = true;
    status_color = "green";
  }
  else if (hasStopSignal) {
    hasError = true;
    status_color = "red";
  }


  if (!isSelectedVehicle) {
    if (resolution > 25) {
      showLabel = false;
      showIcon = false;
      scale = scale / 4;
    }
    else if (resolution > 10) {
      //showLabel = false;
      labelText = publicCode;
      textScale = 0.7;
      scale = scale / 2.5;
    }
    else if (resolution > 8) {
      labelText = publicCode;
      textScale = 0.8;
      scale = scale / 1.5;
    }
    else if (resolution > 4) {
      scale = scale / 1.5;
      textScale = 0.8;
    } else {
      scale = scale / 1.5;
      //textScale=0.8;
    }
  }

  if (isTouchDevice) feature_scale = feature_scale * 1.3;
  if (vehicleStatus == "PARKED") showLabel = false;

  var alertStyle = new Icon(({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    src: "ring.svg",
    scale: scale * feature_scale,
    rotateWithView: true,
    color: status_color
  }))


  //Build styles
  var vehicleStyle = null;
  if (showIcon) vehicleStyle = new Icon(({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    src: vehicleIcon,
    scale: scale * vehicleIconScale * feature_scale,
    rotateWithView: false,
    opacity: vehicleIconOccupacy
  }))

  var vehicleTypeStyle = new CircleStyle({
    radius: 26 * feature_scale * scale,
    fill: new Fill({
      color: color
    })
  });

  var textStyle = null;
  if (showLabel) textStyle = new TextStyle({
    fill: textColor,
    textAlign: 'left',
    scale: textScale * 1.6,
    offsetY: 3,
    offsetX: 35 * scale * feature_scale,
    text: labelText,
    backgroundFill: backgroundColor,
    padding: [3, 3, 0, 3]
  });

  var show = true
  var isOld = false;
  var isStale = false;
  var isParked = false;


  if ((vehicleFilter == "UNKNOWN") && ((telemetryType == "SIS") || (telemetryType == "HYBRID") || (telemetryType == "ITXPT") || (telemetryType == "VIX"))) show = false;
  else if ((vehicleFilter != "ALL") && (telemetryType != vehicleFilter)) show = false;

  if ((statusFilter != "OFFLINE") && (isOld || (vehicleStatus == "OFFLINE"))) show = false;
  else if ((statusFilter == "OFFLINE") && (vehicleStatus != "OFFLINE")) show = false;
  else if ((statusFilter == "ON_DUTY") && (vehicleStatus != "SIGNON")) show = false;
  else if ((statusFilter == "PARKED") && (vehicleStatus != "PARKED")) show = false;
  else if ((statusFilter == "OFF_DUTY") && (vehicleStatus != "NO_SIGNON")) show = false;
  else if ((statusFilter == "ERROR") && ((vehicleStatus == "NO_SIGNON") || (vehicleStatus == "SIGNON") || (vehicleStatus == "PARKED"))) show = false;

  if ((vehicleStatus == "OFFLINE") || (vehicleStatus == "PARKED")) hasError = false;


  var styleArray = [];


  if (show) {
    styleArray = [
      new Style({
        image: vehicleTypeStyle
      }),
      new Style({
        image: vehicleStyle,
        text: textStyle
      })
    ]
    if (hasError) {
      styleArray.push(new Style({
        image: alertStyle
      }))
    }
  }

  return styleArray;
}


initMap();

var socket = io.connect();
var isLoaded = false;
var byteCounter = 0;

function processData(changes) {
  byteCounter = byteCounter + JSON.stringify(changes).length;
  changes.forEach(change => {
    change.vehicleRef = change.R;
    delete change.R;
    if (change.L) {
      change.vehicleLocation = change.L;
      delete change.L;
    }

    var vehicleRef = change.vehicleRef;

    var feature = vectorSource.getFeatureById(vehicleRef);
    if (!feature) {
      feature = new Feature(change);
      feature.setId(vehicleRef);
      vectorSource.addFeature(feature);
      feature = vectorSource.getFeatureById(vehicleRef);
    } else {
      feature.setProperties(change);
    }

    if (change.vehicleLocation) {
      var location = geohash.decode(change.vehicleLocation);
      var newPosition = new Point(ol.proj.fromLonLat([location.longitude, location.latitude]));
      feature.setGeometry(newPosition);
      //window;
    }


    //console.log(vehicleRef);
  })
}

function processReload(changes) {
  isLoaded = false;
  processData(changes);
  isLoaded = true;
}


function processChanges(changes) {
  if (isLoaded) processData(changes);
}


//socket.on('vehicle', updateVehicle);
socket.on('changes', processChanges);
socket.on('reload', processReload);
socket.on('restart', () => {
  document.location.reload(true);
});


function isOlderThan(timestamp, seconds) {
  var _timestamp = new Date(timestamp);
  var now = new Date();
  var diff = (now - _timestamp) / 1000;
  if (diff > seconds) return true;
  else return false;
}

function reload(vehicles) {
  vehicles.forEach(function (vehicle) {
    //updateVehicle(vehicle);
    console.log(vehicles)
  });
}



function reconnect() {
  //vectorSource.clear();
  socket.emit("recon", true);
};

document.addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'visible') {
    reconnect();
  }
});

setInterval(() => {
  document.getElementById("posStats").innerText = Math.round(byteCounter / 1024 / 2) + " kb/s";
  byteCounter = 0;
}, 2000)