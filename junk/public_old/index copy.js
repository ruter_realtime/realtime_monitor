const Circle = ol.geom.Circle;
const RegularShape = ol.style.RegularShape;
const Feature = ol.Feature;
const Point = ol.geom.Point;
const VectorSource = ol.source.Vector;
const VectorLayer = ol.layer.Vector;
const Style = ol.style.Style;
const CircleStyle = ol.style.Circle;
const PolygonStyle = ol.style.Polygon;
const TextStyle = ol.style.Text;
const Fill = ol.style.Fill;
const Stroke = ol.style.Stroke;
const Icon = ol.style.Icon;
const Select = ol.interaction.Select;
const Color = ol.color;
const LineString = ol.geom.LineString;

var map;
var vectorSource;
var journeyPatternSource;
var selectedVehicle;
var isTouchDevice = (navigator.maxTouchPoints > 1);

var vehicleFilter = "ALL";
var statusFilter = "ALL";

function setFilter(type, value) {
  if (type == "vehicle") vehicleFilter = value;
  else if (type == "status") statusFilter = value;
  vectorSource.changed();
}



function colorWithAlpha(color, alpha) {
  const [r, g, b] = Array.from(Color.asArray(color));
  return Color.asString([r, g, b, alpha]);
}



function initMap() {



  var tileLayerOSM = new ol.layer.Tile({
    preload: 12,
    source: new ol.source.OSM()
  });

  map = new ol.Map({
    target: 'map',
    layers: [
      tileLayerOSM
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([10.732747, 59.910946]),
      zoom: 13,
      enableRotation: false
    })
  });

  vectorSource = new VectorSource({
    features: []

  });

  var vectorLayer = new VectorLayer({
    source: vectorSource,
    style: markerStyleFunction
  });

  map.addLayer(vectorLayer);

  journeyPatternSource = new VectorSource({
    features: []

  });

  var journeyPatternLayer = new VectorLayer({
    source: journeyPatternSource,
    style: new ol.style.Style({
      stroke: new ol.style.Stroke({ color: 'royalblue', width: 4 })
    })
  });

  map.addLayer(journeyPatternLayer);


  var selectSingleClick = new Select({
    style: markerStyleFunction
  });
  map.addInteraction(selectSingleClick);
  selectSingleClick.on('select', function (e) {

    if (e.selected[0]) {
      selectedVehicle = e.selected[0].id_;
      console.log("Selected vehicle: " + selectedVehicle);
    }
    else selectedVehicle = null;
  });
  
  
}



function markerStyleFunction(feature, resolution) {

  var vehicleRef = feature.id_;

  var hasSelectedVehicle = false;
  var isSelectedVehicle = false;
  if (selectedVehicle) {
    hasSelectedVehicle = true;
    if (selectedVehicle == vehicleRef) isSelectedVehicle = true;
  }



  var stopPlaceName = feature.get("stopPlaceName");
  var stopSignal = feature.get("stopSignal");
  var doorsOpen = feature.get("doorsOpen");
  var type = feature.get("type");
  var status = feature.get("status");
  var warningLevel = feature.get("warningLevel");
  var lastPosition = feature.get("lastPosition");
  var labelText = feature.get("labelText");
  var signonOld = feature.get("signonOld");
  var stopPlaceLatitude = feature.get("stopPlaceLatitude");
  var stopPlaceLongitude = feature.get("stopPlaceLongitude");
  var isStopped=feature.get("isStopped");
  var lastModeChange=feature.get("lastModeChange");;
  var timeout = 600;

  if ((type == "HYBRID") || (type == "ITXPT")) timeout = 60;
  var isStale = isOlderThan(lastPosition, timeout);
  var isOld = isOlderThan(lastPosition, 1200);
  var isParked=false;
  
  if(isOlderThan(lastModeChange,1200)&&isStopped) {
    isParked=true;
    warningLevel=0;
  }

  textScale = 1;
  //color = 'red';
  feature_scale = 1;
  textColor = new Fill({
    color: 'black'
  });
  backgroundColor = new Fill({
    color: 'rgba(255, 255, 255, 0.5)'
  });

  var color = 'blue';
  if (isStale) color = 'white';
  else if (isParked) color = 'gray';
  else if (warningLevel == 3) color = 'red';
  else if (warningLevel == 2) color = 'orange';
  else if (warningLevel == 1) color = 'yellow';
  else if (labelText == "Ikke i trafikk") color = 'darkgray';
  else if (type == "UNKNOWN") color = 'purple';

  if ((warningLevel == 0) || !warningLevel) {
    if (isStale) color = colorWithAlpha(color, 0.8);
    else if (type == "ITXPT")
      color = colorWithAlpha(color, 0.9);
    else if (type == "HYBRID") color = colorWithAlpha(color, 0.6);
    else if (type == "SIS") color = colorWithAlpha(color, 0.3);
    else color = colorWithAlpha(color, 0.2);
  }
  else color = colorWithAlpha(color, 0.7);

  scale = 0.7;

  if (isStale) labelText = "OFFLINE";
  else if(isParked)labelText = "PARKED";
  else if (signonOld) labelText = "NO SIGNON LAST 24H";
  else if (warningLevel == 3) labelText = "FAILED SIGNON";
  else if (warningLevel == 2) labelText = "NO ETA UPDATES";

  var showLabel = true;

  var vehicleIconOccupacy = 1;
  if (hasSelectedVehicle) {
    document.getElementById("overlay").style.display = "block";

    if (!isSelectedVehicle) {
      color = 'gray';
      feature_scale = 0.7;
      showLabel = false;
      vehicleIconOccupacy = 0.5;
    } else {
      feature_scale = 1.1;

      var typeElement = document.getElementById("type");
      typeElement.style.color = "blue";
      typeElement.innerText = type;
      document.getElementById("id").innerText = vehicleRef;
      var statusText = ""
      if ((warningLevel > 0) || (labelText == "OFFLINE")) {
        document.getElementById("display").innerText = feature.get("labelText") + " (last known)"
        statusText = labelText;
      } else {
        document.getElementById("display").innerText = labelText;
        if (doorsOpen) {
          statusText = stopPlaceName + " > (Doors open)";
        }
        else if (stopSignal) {
          statusText = stopPlaceName + " > (Stopping)";
        }
        else statusText = stopPlaceName;
      }

      //if ((status == "UNKNOWN") || (status == "OK") || (status === undefined)) statusText = ""
      document.getElementById("status").innerText = statusText;
      document.getElementById("status").style.color = "gray";

      if (stopPlaceLatitude && stopPlaceLongitude) {
        var stopPlace = ol.proj.fromLonLat([stopPlaceLongitude, stopPlaceLatitude]);
        var vehiclePoint = feature.getGeometry().getCoordinates();
        var nextStopLine = [stopPlace, vehiclePoint];

        var featureLine = journeyPatternSource.getFeatureById("nextStopLine");
        if (featureLine) {
          featureLine.setGeometry(new LineString(nextStopLine));
        }
        else {
          featureLine = new ol.Feature({
            geometry: new LineString(nextStopLine)
          });

          featureLine.setId("nextStopLine");
          journeyPatternSource.addFeature(featureLine);
        }
      }
    }
  } else {
    document.getElementById("overlay").style.display = "none";;
    journeyPatternSource.clear();
  }







  var hasError = false;
  var status_color = "black";
  if (doorsOpen) {
    status_color = 'green';
    hasError = true;
  }
  else if (stopSignal) {
    status_color = 'red';
    hasError = true;
  }



  if (resolution > 300) {
    showLabel = false;
    scale = scale / 4;
  }
  else if (resolution > 10) {
    showLabel = false;
    scale = scale / 2;
  }
  else if (resolution > 4) {
    scale = scale / 1.5;
  }

  if (isTouchDevice) feature_scale = feature_scale * 1.3;


  var alertStyle = new Icon(({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    src: "ring.svg",
    scale: scale * feature_scale,
    rotateWithView: true,
    color: status_color
  }))

  var vehicleTypeId = vehicleRef.substr(0, 1);
  var vehicleType = "bus.svg";
  var vehicleScale = 1;
  if (vehicleTypeId == "2") {
    vehicleType = "tram.svg";
    vehicleScale = 1.3;
  }
  else if (vehicleTypeId == "9") {
    vehicleType = "ferry.svg";
    vehicleScale = 1.2;
  }

  //Build styles
  vehicleStyle = new Icon(({
    anchor: [0.5, 0.5],
    anchorXUnits: 'fraction',
    anchorYUnits: 'fraction',
    src: vehicleType,
    scale: scale * vehicleScale * feature_scale,
    rotateWithView: false,
    opacity: vehicleIconOccupacy
  }))

  var vehicleTypeStyle = new CircleStyle({
    radius: 26 * feature_scale * scale,
    fill: new Fill({
      color: color
    })
  });

  var textStyle = null;
  if (showLabel) textStyle = new TextStyle({
    fill: textColor,
    textAlign: 'left',
    scale: textScale * 1.6,
    offsetY: 3,
    offsetX: 35 * scale * feature_scale,
    text: labelText,
    backgroundFill: backgroundColor,
    padding: [3, 3, 0, 3]
  });

  var show = true


  if (vehicleFilter != "ALL") {
    if (type != vehicleFilter)
      show = false;
  }

  if (statusFilter == "ALL") {
    if (isOld) show = false;
  }
  else if (statusFilter == "ON_DUTY") {
    if (isOld || isStale||isParked) show = false;
    if (warningLevel > 0) show = false;
    if (labelText == "Ikke i trafikk") show = false;
  }
  else if (statusFilter == "OFF_DUTY") {
    if (isOld || isStale) show = false;
    if (warningLevel > 0) show = false;
    if ((labelText != "Ikke i trafikk")&&(labelText != "PARKED"))  show = false;
  }
  else if (statusFilter == "ERROR") {
    if (isOld || isStale) show = false;
    if (warningLevel == 0) show = false;

  }
  else if (statusFilter == "OFFLINE") {
    if (!(isOld || isStale)) show = false;
  }

  if (isOld || isStale||isParked) hasError = false;


  var styleArray = [];
  if (show) {
    styleArray = [
      new Style({
        image: vehicleTypeStyle
      }),
      new Style({
        image: vehicleStyle,
        text: textStyle
      })
    ]
    if (hasError) {
      styleArray.push(new Style({
        image: alertStyle
      }))
    }
  }

  return styleArray;
}


initMap();


var vehicles = new Map();

function updateVehicle(vehicle) {
  if (vehicle.position) {
    var vehicleRef = vehicle.vehicleRef;

    var feature = vectorSource.getFeatureById(vehicleRef);
    if (!feature) {
      var _vehicle = [vehicle.position.lastPositionRecord];
      updatePositions(_vehicle);
      feature = vectorSource.getFeatureById(vehicleRef);
    }
    var type = feature.get("type");
    if (vehicle.type) type = vehicle.type;

    var labelText = vehicleRef;
    if (vehicle.diagnostics)
      if (vehicle.diagnostics.externaldisplay)
        if (vehicle.diagnostics.externaldisplay.dpi)
          if (vehicle.diagnostics.externaldisplay.dpi.destination == "Ikke i trafikk") labelText = vehicle.diagnostics.externaldisplay.dpi.destination;
          else labelText = vehicle.diagnostics.externaldisplay.dpi.publicCode + " " + vehicle.diagnostics.externaldisplay.dpi.destination;

    var stopSignal = null;
    if (vehicle.diagnostics2)
      if (vehicle.diagnostics2.hasStopSignal)
        stopSignal = vehicle.diagnostics2.sensors.stopSignal.value;


    var doorsOpen = null;
    if (vehicle.diagnostics2)
      if (vehicle.diagnostics2.hasDoorsOpen)
        if (vehicle.doorSensorStatus.valid)
          doorsOpen = vehicle.diagnostics2.sensors.doorsOpen.value;
        else
          doorsOpen = undefined;


    var warningLevel = 0;
    if (vehicle.eta)
      if (vehicle.eta.isOverDueHigh)
        warningLevel = 2;

    if (vehicle.signon)
      if (vehicle.signon.signonFailed)
        warningLevel = 3;


    var signonOld = false;
    if (vehicle.signon)
      if (vehicle.signon.isOutdated) {
        signonOld = true;
        warningLevel = 1;
      }

    var stopPlaceLongitude = null;
    try {
      stopPlaceLongitude = vehicle.diagnostics.nextstop.dpi.stopPlace.longitude;
    } catch (err) { }

    var stopPlaceLatitude = null;
    try {
      stopPlaceLatitude = vehicle.diagnostics.nextstop.dpi.stopPlace.latitude;
    } catch (err) { }

    var stopPlaceName = null;
    try {
      stopPlaceName = vehicle.diagnostics2.nextStop.dpiNextStop.stopPointName;
    } catch (err) { }

    var isStopped=vehicle.position.isStopped;
    var lastModeChange=vehicle.position.lastModeChange;

    //FIX TYPE BUG
    if (type == "IPXPT")
      type = "ITXPT";

    feature.setProperties({
      type: type,
      status: status,
      labelText: labelText,
      stopSignal: stopSignal,
      doorsOpen: doorsOpen,
      warningLevel: warningLevel,
      signonOld: signonOld,
      stopPlaceLongitude: stopPlaceLongitude,
      stopPlaceLatitude: stopPlaceLatitude,
      stopPlaceName: stopPlaceName,
      isStopped:isStopped,
      lastModeChange:lastModeChange
    });
    vehicles.set(vehicleRef, vehicle);

  }
}

var socket = io.connect();

var posLagSum = 0;
var posLagCount = 0;
var posStatsStarted = false;
var posCount = 0;

setInterval(() => {
  var posSpeed = Math.round(posCount / 2.5);
  var posLag = posLagSum / posLagCount;
  posLagSum = 0;
  posCount = 0;
  posLagCount = 0;
  if (posStatsStarted) document.getElementById("posStats").innerText = posSpeed + "pos/s - " + posLag.toFixed(1) + "s avg. lag";
  else posStatsStarted = true;
}, 2500);

socket.on('vehicle', updateVehicle);
socket.on('positions', updatePositions);
socket.on('reload', reload);
socket.on('restart', () => {
  document.location.reload(true);
});


function isOlderThan(timestamp, seconds) {
  var _timestamp = new Date(timestamp);
  var now = new Date();
  var diff = (now - _timestamp) / 1000;
  if (diff > seconds) return true;
  else return false;
}

function reload(vehicles) {
  vehicles.forEach(function (vehicle) {
    updateVehicle(vehicle);
  });
}

function updatePositions(message) {
  message.forEach(position => {
    posCount++;

    if ((position[3] != "SIS") && (position[3] != "AIS")) {
      var posLag = ((new Date() - new Date(position[2])) / 1000);
      posLagSum = posLagSum + posLag;
      posLagCount++;
    }


    var vehicleRef = position[0];
    var location = geohash.decode(position[1]);

    var newPosition = new Point(ol.proj.fromLonLat([location.longitude, location.latitude]));

    var feature = vectorSource.getFeatureById(vehicleRef);
    var type = position[3];

    //FIX TYPE BUG
    if (type == "IPXPT")
      type = "ITXPT";

    if (!feature) {
      feature = new Feature({
        display: vehicleRef,
        geometry: newPosition,
        type: type,
        status: "UNKNOWN",
        vehicleRef: vehicleRef,
        labelText: vehicleRef,
        lastPosition: new Date(position[2])
      });
      feature.setId(vehicleRef);
      vectorSource.addFeature(feature);
    } else {
      feature.setProperties({
        type: type,
        lastPosition: new Date(position[2])
      });
    }
    feature.setGeometry(newPosition);
    window;
  });

  

  

  //export { reconnect }; 

 
  /*window.focus(()=>{
    
  });*/
  
  //console.log(location);
}

function reconnect(){
  //vectorSource.clear();
  socket.emit("recon",true);
};

document.addEventListener('visibilitychange', () => {
  if (document.visibilityState === 'visible') {
    reconnect();
  }
});
