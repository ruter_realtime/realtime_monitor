'use strict';
const diff = require("deep-object-diff").diff;
const deepmerge = require("deepmerge");

//Internal state
var io;
var logger;
const fieldLookup = {};

var transmissionTimer;
var transmissionTimerInterval;




function setup(logger, io, fieldList, transmissionInterval) {
    io = io;
    logger = logger;
    transmissionTimerInterval = transmissionInterval || 1000;
    if (Array.isArray(fieldList)) {
        fieldList.forEach(field => {
            addField(field);
        });
    }
}

let started = false;
function start(io) {
    started = true;
    reTransmit(io);
    io.on('connection', (socket) => {
        console.log('A user is connected')
        if (started) {
            reTransmit(socket);
            socket.on('restart', function (message) {
                console.log("User requested restart");
                reTransmit(socket);
            });
        }
    })

    transmissionTimer = setInterval(() => {
        transmit(transmitQueue,io);
    }, transmissionTimerInterval)
}

function stop() {
    started = false;
    clearInterval(transmissionTimer);
}

let nextSequenceNumber = 0;
const transmitQueue = [];

function transmit(queue,io) {
    if (transmitQueue.length > 0) {
        let message = "^S|" + nextSequenceNumber;
        queue.forEach(record => {
            message = message + record;
        });
        transmitQueue.length = 0;
        nextSequenceNumber++;
        if (nextSequenceNumber == 100) nextSequenceNumber = 0;
        //console.log(message);
        io.emit('updates', message);
    }
}

const recordState = new Map();

function reTransmit(socket) {
    let message = "^X|" + nextSequenceNumber;
    if (Object.entries(fieldLookup).length > 0) {
        Object.entries(fieldLookup).forEach(([fieldId, field]) => {
            message = message + encodeFieldLookup(field, fieldId);
        })
    }
    if (recordState.size > 0) {
        recordState.forEach((value, key) => {
            message = message + encodeRecord(key, value);
        })
    }
    //console.log(message);
    socket.emit('updates', message);
}

function transmitRecordUpdate(key, newRecord) {

    //Array merge function: Do not allow arrays in records
    function deleteMerge(destinationArray, sourceArray, options) {
        return null;
    }

    const oldRecord = recordState.get(key);

    let mergedRecord;
    if (oldRecord) mergedRecord = deepmerge(oldRecord, newRecord, { arrayMerge: deleteMerge });
    else mergedRecord = newRecord;

    let diffRecord;
    if (oldRecord) diffRecord = diff(oldRecord, newRecord);
    else diffRecord = newRecord;

    recordState.set(key, mergedRecord);
    transmittRecordUpdate(key, diffRecord);
}

let nextFieldID = 1;
function addField(field) {
    const lookup = field.toString();
    if (!fieldLookup[lookup]) {
        const fieldId = nextFieldID
        fieldLookup[lookup] = fieldId;
        transmitFieldLookup(lookup, fieldId)
        nextFieldID++;
        return true;
    } else return false;
}

function lookupOrAddField(field) {
    const lookup = field.toString();
    if (!fieldLookup[lookup]) addField(lookup);
    return fieldLookup[lookup];
}

function transmitFieldLookup(field, fieldId) {
    transmitQueue.push(encodeFieldLookup(field, fieldId));
}

function transmittRecordUpdate(key, record) {
    const encodedRecord = encodeRecord(key, record);
    if (encodedRecord) transmitQueue.push(encodedRecord);
}

function encodeFieldLookup(field, fieldId) {
    return "^F|" + fieldId + "|" + field;
}

function encodeRecord(key, record) {
    const shortKey = lookupOrAddField(key);
    let result;
    let encodedFields = 0

    result = "^R|" + shortKey;
    Object.entries(record).forEach(([fieldKey, value]) => {
        if (value !== undefined) {
            if ((!value) && (value !== 0)) value = null;
            const fieldId = lookupOrAddField(fieldKey);
            result = result + "|" + fieldId + "|" + value;
            encodedFields++;
        }
    });
    if (!encodedFields) result = null;
    console.log(result);
    return result;
}

/*setup(1, 2, ["Name", "Place"]);
transmitRecordUpdate("898797", { Name: "Henrik", Age: 51, Place: "Oslo" });
transmitRecordUpdate("898797", { Place: "Oslo" });
transmitRecordUpdate("898797", { Age: 52, Place: "Oslo" });
transmitRecordUpdate("898797", { Name: null, Age: 51, Place: "Oslo" });
console.log(transmitQueue);
reTransmit(1)
start();*/

module.exports = {
    setup: setup,
    start: start,
    stop: stop,
    transmitRecordUpdate: transmitRecordUpdate,
};