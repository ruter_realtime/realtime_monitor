const EventEmitter = require('events');
const StateManager = require("kafka-state-manager").StateManager; // Kafka pakage
const turf = require('@turf/turf')

String.prototype.toCamelCase = function () {
    return this.replace(/^([A-Z])|\s(\w)/g, function (match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();
    });
};


class State extends EventEmitter {
    constructor(vehicleProcessor, logger) {
        super();
        const self = this;
        this.vehicleProcessor = vehicleProcessor;

        this.stateManager = new StateManager(
            {
                groupId: "realtime_monitor_state",
                clientId: "realtime_monitor1",
                brokerList: ["10.10.0.13:9092"],
                schemaRegistryUrl: "http://10.10.0.13:8081",
                queueSize: 500, //2500 to 5000 is probably a good number here for load performance and latency performance balanceing
                compression: "none",
                statsLogIntervalSec: 20,
                stateLogIntervalSec: 20,
                //memoryLogIntervalSec: 20,
                states: [
                    { stateName: "vehicle", changeHandler: changeHandlerVehicle },
                    { stateName: "line" },
                    { stateName: "block", cleaner: transactionalEntityCleaner },
                    { stateName: "journey", cleaner: transactionalEntityCleaner },
                    { stateName: "quay" },
                    { stateName: "journeypattern" },
                    { stateName: "servicelink" },
                    { stateName: "actualblock", cleaner: transactionalEntityCleaner },
                    { stateName: "actualjourney", cleaner: transactionalEntityCleaner },
                    { stateName: "actualcall", changeHandler: changeHandlerActualCall, cleaner: transactionalEntityCleaner },
                    { stateName: "assignment", cleaner: transactionalEntityCleaner },
                ]
            }
        );
        let runCleaner = false;

        async function changeHandlerVehicle(stateName, stateKey, stateRecord) {
            if (runCleaner)
                self.vehicleProcessor(stateRecord);
        }

        async function changeHandlerActualCall(stateName, stateKey, stateRecord) {
            if (((stateRecord.status == "ARRIVED") || (stateRecord.status == "APPROACHING")) && runCleaner) {

                if (stateRecord.vehicleRef) {
                    const vehicle = {
                        key: stateRecord.vehicleRef,
                        timestamp: stateRecord.timestamp,
                        currentActualJourneyRef: stateRecord.actualJourneyRef,
                        currentJourneyCode: stateRecord.journeyCode,
                        currentJourneyRef: stateRecord.journeyRef,
                        currentJourneyPatternRef: stateRecord.journeyPatternRef,
                        currentJourneyType: stateRecord.journeyType,
                        currentLineRef: stateRecord.lineRef,
                        currentActualCall: {
                            order: stateRecord.order,
                            quayRef: stateRecord.quayRef,
                            actualCallRef: stateRecord.key,
                            isFirstInActualJourney: stateRecord.isFirstInActualJourney,
                            isLastInActualJourney: stateRecord.isLastInActualJourney,
                            plannedArrivalTime: stateRecord.plannedArrivalTime,
                            estimatedArrivalTime: stateRecord.estimatedArrivalTime,
                            actualArrivalTime: stateRecord.actualArrivalTime,
                            plannedDepartureTime: stateRecord.plannedDepartureTime,
                            estimatedDepartureTime: stateRecord.estimatedDepartureTime,
                            actualDepartureTime: stateRecord.actualDepartureTime,
                            status: stateRecord.status,
                            doneStatus: stateRecord.doneStatus,
                            stopPointBehaviourType: stateRecord.stopPointBehaviourType,

                        },
                        status: "ON_DUTY"
                    }

                    let quay = null;
                    if (stateRecord.quayRef) {
                        quay = await self.stateManager.get("quay", stateRecord.quayRef);
                        if (quay) {
                            vehicle.currentActualCall.quayName = quay.name;
                            vehicle.currentActualCall.quayLocation = quay.location;
                            //console.log(quay.name);
                        }

                    };

                    self.stateManager.merge("vehicle", stateRecord.vehicleRef, vehicle);
                }
                //console.log(JSON.stringify(stateRecord));
            }

        }

        async function transactionalEntityCleaner(stateName, stateKey, stateRecord, state) {
            let result = true;
            if (runCleaner) {
                const now = new Date();
                let then;
                if (stateRecord?.updateTimestamp) then = new Date(stateRecord.updateTimestamp);
                else then = new Date(stateRecord.timestamp);
                const diff = (now - then);
                if (diff > 60000) {
                    if ((stateName == "actualblock") && (stateRecord.status == "DONE"))
                        result = false;
                    else if (stateName == "actualjourney") {
                        if (stateRecord.actualBlockRef) {
                            const actualBlockExists = state.get("actualblock").has(stateRecord.actualBlockRef);
                            if (!actualBlockExists)
                                result = false;
                        }
                    }
                    else if (stateName == "actualcall") {
                        if (stateRecord.actualJourneyRef) {
                            const actualJourneyExists = state.get("actualjourney").has(stateRecord.actualJourneyRef);
                            if (!actualJourneyExists)
                                result = false;
                        }
                    }
                }

                if (diff > (60000 * 60 * 24))
                    result = false;

            }
            return result;
        }



        this.stateManager.connect().then(async () => {

            await this.stateManager.subscribe([
                {
                    topic: "STAGE.entity.assignment.key",
                    mapper: assignmentMapper,
                    //stateName: "actualCall",
                    keepOffsetOnReassign: true,
                    startFromLatest: false,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                    maxAgeSec: 3600 * 24
                }
            ]);



            await this.stateManager.subscribe([
                {
                    topic: "STAGE.entity.pass.key",
                    mapper: passMapper,
                    //stateName: "actualCall",
                    keepOffsetOnReassign: true,
                    startFromLatest: false,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                    maxAgeSec: 3600 * 4
                }
            ]);



            await this.stateManager.subscribe([
                {
                    topic: "STAGE.entity.actualcall.estimatedtime",
                    mapper: etaMapper,
                    keepOffsetOnReassign: true,
                    startFromLatest: false,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                    maxAgeSec: 3600 * 4
                }
            ]);

            await this.stateManager.subscribe([
                {
                    topic: "STAGE.data.vehicle-topic.externaldisplay-dpi",
                    mapper: externalDisplayMapper,
                    //stateName: "actualCall",
                    keepOffsetOnReassign: true,
                    startFromLatest: false,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                    maxAgeSec: 3600 * 24
                }
            ]);

            /*await this.stateManager.subscribe([
                {
                    topic: "entity.vehicle.position",
                    mapper: positionMapper,
                    //stateName: "actualCall",
                    keepOffsetOnReassign: true,
                    startFromLatest: true,    // Starts from latest message on connect
                    messageDecoder: "JSON", // AUTO,JSON,AVRO,STRING,BINARY
                }
            ]);*/


            runCleaner = true;
        });
    }
}

async function positionMapper(states, message) {
    let result = [];
    const vehicleRef = message.value.key;
    const operatorName = message.value.operatorRef;

    const timestamp = message.value.position.timestamp;
    const longitude = message.value.position.longitude;
    const latitude = message.value.position.latitude;
    const locationSource = message.value.source;
    const platformType = message.value.vehicleType;
    const isStopped = message.value.position.isStopped;
    const lastModeChangeTimestamp = message.value.position.lastModeChangeTimestamp;

    const vehicleRecord = {
        stateName: "vehicle",
        stateKey: vehicleRef,
        stateRecord: {
            updateTimestamp: timestamp,
            key: vehicleRef,
            longitude: longitude,
            latitude: latitude,
            locationSource: locationSource,
            platformType: platformType,
            isStopped: isStopped,
            lastModeChangeTimestamp: lastModeChangeTimestamp
        },
        merge: true
    }
    result.push(vehicleRecord);

    return result;
}


async function externalDisplayMapper(states, message) {
    let result = [];
    let keyParts = message.key.split("/");
    const vehicleRef = keyParts[2];
    const operatorName = keyParts[0];

    const timestamp = message.value.eventTimestamp;
    const externalDisplayCode = message.value.publicCode || null;
    const externalDisplayText = message.value.destination;
    const externalDisplaySubText = message.value.alternativeMessage || null;

    const vehicleRecord = {
        stateName: "vehicle",
        stateKey: vehicleRef,
        stateRecord: {
            updateTimestamp: timestamp,
            externalDisplayTimestamp: timestamp,
            key: vehicleRef,
            operatorName: operatorName,
            externalDisplayCode: externalDisplayCode,
            externalDisplayText: externalDisplayText,
            externalDisplaySubText: externalDisplaySubText,
            externalDisplaySource: "DPI"
        },
        merge: true
    }
    result.push(vehicleRecord);


    return result;
}

async function assignmentMapper(states, message) {
    let result = [];
    const vehicleRef = message.value.entityData.vehicleRef;
    const timestamp = message.value.entityHeader.eventTimestamp;
    const key = message.value.entityHeader.key;
    const record = message.value.entityData;
    if ((message?.value?.entityData?.attempt?.type == "SIGNON") && (message?.value?.entityData?.attempt?.status == "SUCCESS")) {



        let type = "FROM_START";
        if (record.intention.journeySequence[0].order > 1) type = "PARTIAL";

        const vehicleRecord = {
            stateName: "vehicle",
            stateKey: vehicleRef,
            stateRecord: {
                timestamp: timestamp,
                key: vehicleRef,
                operatorName: record.operatorRef,
                lastSigninActualBlock: record.intention.actualBlockRef,
                lastSigninBlockCode: record.attempt.triggerBlockRef.split("-")[0],
                lastSigninBlockRef: record.attempt.triggerBlockRef,
                lastSigninActualJourney: record.intention.journeySequence[0].actualJourneyRef,
                lastSigninJourneyCode: record.intention.journeySequence[0].hastusShortKey,
                lastSigninJourneyRef: record.intention.journeySequence[0].hastusLongKey,
                lastSigninType: type,
                status: "ASSIGNED"
            },
            merge: true
        }
        result.push(vehicleRecord);

        const actualBlockRecord = {
            stateName: "actualblock",
            stateKey: record.intention.actualBlockRef,
            stateRecord: {
                timestamp: timestamp,
                updateTimestamp: timestamp,
                key: record.intention.actualBlockRef,
                blockCode: record.attempt.triggerBlockRef,
                blockRef: record.attempt.triggerBlockRef + "-" + record.intention.startTime,
                plannedStartTime: record.intention.startTime,
                plannedEndTime: record.intention.endTime,
                startQuayRef: record.intention.journeySequence[0].startQuayRef,
                endQuayRef: record.intention.journeySequence[record.intention.journeySequence.length - 1].endQuayRef,
                numberOfJourneys: record.intention.journeySequence.length,
                operatingDate: record.intention.operatingDay,
                operatorName: record.operatorRef,
                vehicleRef: vehicleRef,
                signinType: type,
                firstJourneyOrderInBlock: record.intention.journeySequence[0].order,
                status: "ASSIGNED",
                doneStatus: null

            },
            merge: true
        }
        result.push(actualBlockRecord);

        const blockRecord = {
            stateName: "block",
            stateKey: record.attempt.triggerBlockRef + "-" + record.intention.startTime,
            stateRecord: {
                timestamp: timestamp,
                key: record.attempt.triggerBlockRef + "-" + record.intention.startTime,
                blockCode: record.attempt.triggerBlockRef,
                operatingDate: record.intention.operatingDay,
                operatorName: record.operatorRef
            },
            merge: true
        }
        result.push(blockRecord);

        let _previousActualJourneyRef = null;
        let _previousActualCallRef = null;

        let journeyIndex = -1;
        const lastJourneyIndex = record.intention.journeySequence.length - 1;

        record.intention.journeySequence.forEach((journey) => {
            const journeyRef = journey.hastusLongKey;
            let type = "FROM_START";
            if (journey.stopPointSequence[0].order > 1) type = "PARTIAL";

            let lineRef = journey.line.lineRef;
            if (lineRef == "RUT:Line:0") {
                lineRef = null;
            }

            journeyIndex++;

            let previousActualJourneyRef = _previousActualJourneyRef;
            _previousActualJourneyRef = journey.actualJourneyRef;

            let isFirstInActualBlock = false;
            let isLastInActualBlock = false;

            if (journeyIndex == 0) isFirstInActualBlock = true;
            else if (journeyIndex == lastJourneyIndex) isLastInActualBlock = true;

            let nextActualJourneyRef = null;
            if (journeyIndex < lastJourneyIndex) nextActualJourneyRef = record.intention.journeySequence[journeyIndex + 1].actualJourneyRef

            const actualJourneyRecord = {
                stateName: "actualjourney",
                stateKey: journey.actualJourneyRef,
                stateRecord: {
                    timestamp: timestamp,
                    updateTimestamp: timestamp,
                    key: journey.actualJourneyRef,
                    journeyRef: journey.hastusLongKey,
                    journeyCode: journey.hastusShortKey,
                    journeyPatternRef: journey.journeyPatternRef,
                    actualBlockRef: record.intention.actualBlockRef,
                    blockRef: record.attempt.triggerBlockRef,
                    plannedStartTime: journey.startTime,
                    plannedEndTime: journey.endTime,
                    startQuayRef: journey.startQuayRef,
                    endQuayRef: journey.endQuayRef,
                    numberOfStopPoints: journey.stopPointSequence.length,
                    operatingDate: journey.startTime.substr(0, 10),
                    vehicleRef: vehicleRef,
                    lineRef: journey.line.lineRef,
                    order: journey.order,
                    journeyType: journey.journeyType.toUpperCase(),
                    signinType: type,
                    firstStopPointOrderInJourney: journey.stopPointSequence[0].order,
                    previousActualJourneyRef: previousActualJourneyRef,
                    nextActualJourneyRef: nextActualJourneyRef,
                    isFirstInActualBlock: isFirstInActualBlock,
                    isLastInActualBlock: isLastInActualBlock,
                    status: "ASSIGNED",
                    doneStatus: null
                },
                merge: true
            }
            result.push(actualJourneyRecord);

            const journeyRecord = {
                stateName: "journey",
                stateKey: journey.hastusLongKey,
                stateRecord: {
                    timestamp: timestamp,
                    key: journey.hastusLongKey,
                    journeyCode: journey.hastusShortKey,
                    journeyPatternRef: journey.journeyPatternRef,
                    plannedStartTime: journey.startTime,
                    plannedEndTime: journey.endTime,
                    startQuayRef: journey.startQuayRef,
                    endQuayRef: journey.endQuayRef,
                    numberOfStopPoints: journey.stopPointSequence.length,
                    lineRef: journey.line.lineRef,
                    journeyType: journey.journeyType.toUpperCase(),
                },
                merge: true
            }
            result.push(journeyRecord);


            const lineRecord = {
                stateName: "line",
                stateKey: journey.line.lineRef,
                stateRecord: {
                    timestamp: timestamp,
                    key: journey.line.lineRef,
                    monitor: journey.line.monitor,
                    publicCode: journey.line.publicCode,
                    privateCode: journey.line.privateCode,
                    transportMode: journey.line.transportMode.toUpperCase(),
                    networkColor: journey.line.backgroundColour
                },
                merge: true
            }
            result.push(lineRecord);


            let callIndex = -1;
            const lastCallIndex = journey.stopPointSequence.length - 1;

            const hasJourneyPattern = states.get("journeypattern").has(journey.journeyPatternRef);
            let journeyPatternRecord = null;
            if (!hasJourneyPattern) {
                journeyPatternRecord = {
                    stateName: "journeypattern",
                    stateKey: journey.journeyPatternRef,
                    stateRecord: {
                        timestamp: timestamp,
                        key: journey.journeyPatternRef,
                        startLocation: null,
                        endLocation: null,
                        path: [],
                        stopPoints: []
                    },
                    merge: true
                }

                states.get("journeypattern").set(journey.journeyPatternRef, journeyPatternRecord.stateRecord);

                journeyPatternRecord.stateRecord.path = prosessServiceLinks(journey.serviceLinkSequence, journey.completenessServiceLinkQualityIndicator.incompleteServiceLink, journey.journeyPatternRef);
            }

            journey.stopPointSequence.forEach((stopPoint) => {
                const location = JSON.parse(stopPoint.location)

                let isFirstInActualJourney = false;
                if (stopPoint.order == 1) isFirstInActualJourney = true;

                let isLastInJourney = false;
                if (stopPoint.order == journey.stopPointSequence[journey.stopPointSequence.length - 1].order) isLastInJourney = true;

                if (!hasJourneyPattern) {
                    let stopPointRecord = {
                        order: stopPoint.order,
                        quayRef: stopPoint.quayRef,
                        name: stopPoint.name,
                        quayLocation: location.geometry.coordinates,
                        location: null,
                        detectionRadius: null,
                        entryDirection: null,
                        entryDirectionWindow: null,
                        distanceOnPath: null,
                        isFirst: isFirstInActualJourney,
                        isLast: isLastInJourney
                    }
                    if (journeyPatternRecord.stateRecord?.path?.length > 2) {
                        var line = turf.lineString(journeyPatternRecord.stateRecord.path);
                        var point = turf.point(location.geometry.coordinates);
                        var snapped = turf.nearestPointOnLine(line, point);
                        stopPointRecord.distanceOnPath = snapped.properties.location * 1000
                        stopPointRecord.location = snapped.geometry.coordinates;
                    }
                    journeyPatternRecord.stateRecord.stopPoints.push(stopPointRecord);
                }

                callIndex++;

                previousActualCallRef = _previousActualCallRef;
                _previousActualCallRef = stopPoint.actualCallRef;

                let nextActualCallRef = null;
                if (callIndex < lastCallIndex) nextActualCallRef = journey.stopPointSequence[callIndex + 1].actualCallRef
                else {
                    if (journeyIndex < lastJourneyIndex) {
                        nextActualCallRef = record.intention.journeySequence[journeyIndex + 1].stopPointSequence[0].actualCallRef;
                    }
                }

                const actualCallRecord = {
                    stateName: "actualcall",
                    stateKey: stopPoint.actualCallRef,
                    stateRecord: {
                        timestamp: timestamp,
                        updateTimestamp: timestamp,
                        key: stopPoint.actualCallRef,
                        quayRef: stopPoint.quayRef,
                        actualJourneyRef: journey.actualJourneyRef,
                        journeyRef: journey.hastusLongKey,
                        journeyCode: journey.hastusShortKey,
                        journeyPatternRef: journey.journeyPatternRef,
                        actualBlockRef: record.intention.actualBlockRef,
                        blockRef: record.attempt.triggerBlockRef + "-" + record.intention.startTime,
                        plannedArrivalTime: stopPoint.arrivalTime,
                        plannedDepartureTime: stopPoint.departureTime,
                        isFirstInActualJourney: isFirstInActualJourney,
                        isLastInActualJourney: isLastInJourney,
                        vehicleRef: vehicleRef,
                        lineRef: lineRef,
                        order: stopPoint.order,
                        signinType: type,
                        stopPointBehaviourType: stopPoint.stopPointBehaviourType,
                        journeyType: journey.journeyType.toUpperCase(),
                        previousActualCallRef: previousActualCallRef,
                        nextActualCallRef: nextActualCallRef,
                        status: "ASSIGNED",
                        doneStatus: null
                    },
                    merge: true
                }
                result.push(actualCallRecord);




                const quayRecord = {
                    stateName: "quay",
                    stateKey: stopPoint.quayRef,
                    stateRecord: {
                        timestamp: timestamp,
                        key: stopPoint.quayRef,
                        location: location.geometry.coordinates,
                        name: stopPoint.name,
                        stopPlaceRef: stopPoint.stopPointRef
                    },
                    merge: true
                }
                result.push(quayRecord);
            });



            if (!hasJourneyPattern && (journeyPatternRecord.stateRecord?.path?.length > 2)) {
                const features = [turf.lineString(journeyPatternRecord.stateRecord.path, { journeyPatternRef: journeyPatternRecord.stateRecord.key })];
                journeyPatternRecord.stateRecord.stopPoints.forEach(point => {
                    if (point.location)
                        features.push(turf.point(point.location, { name: point.name }));
                })

                //console.log(JSON.stringify(turf.featureCollection(features)))

                result.push(journeyPatternRecord);

            }

        });
    } else if (message?.value?.entityData?.attempt?.type == "SIGNOFF") {
        const vehicleRecord = {
            stateName: "vehicle",
            stateKey: vehicleRef,
            stateRecord: {
                timestamp: timestamp,
                key: vehicleRef,
                operatorName: record.operatorRef,
                status: "OFF_DUTY"
            },
            merge: true
        }
        result.push(vehicleRecord);
    }


    return result;
}


function prosessServiceLinks(serviceLinkSequence, incompleteServiceLink, journeyPatternRef) {

    //Concatenate all servicelinks in JourneyPattern
    const serviceLinkPaths = [];
    let hasSimple = false;
    //let raw = [];
    serviceLinkSequence.forEach((ServiceLink) => {
        if (ServiceLink.trackLine) {
            const path = JSON.parse(ServiceLink.trackLine).geometry.coordinates;
            const dedupedPath = serviceLinkPaths.push(dedupPath(path));
            if (dedupedPath.length > 2) serviceLinkPaths.push(dedupedPath);
            else hasSimple = true;
        } else hasSimple = true;
    })

    let spliced = [];
    let count2 = 0
    let lastPath = null;
    serviceLinkPaths.forEach((path) => {
        //removeKinks(path);
        if (lastPath && (lastPath.length > 2) && (path.length > 2)) {
            const serviceLinkDistance = turf.distance(lastPath[lastPath.length - 1], path[0]) * 1000;
            /*if (serviceLinkDistance) {
                let count = 0;
                let done = false;
                let removeExit = true;
                while (!done) {
                    const enterBearing = getBearing(lastPath[lastPath.length - 2], lastPath[lastPath.length - 1]);
                    const exitBearing = getBearing(path[0], path[1]);
                    const enterExitDiff = bearingDiff(enterBearing, exitBearing);

                    const enterExitBearing = getBearing(lastPath[lastPath.length - 1], path[0]);
                    const enterDiffDiff = bearingDiff(enterBearing, enterExitBearing);
                    const exitDiffDiff = bearingDiff(enterExitBearing, exitBearing);
                    //const enterExitBearing2 = getBearing(lastPath[lastPath.length - 2], path[1]);
                    //const enterDiffDiff2 = bearingDiff(enterBearing, enterExitBearing2);
                    //const exitDiffDiff2 = bearingDiff(enterExitBearing2, exitBearing);
                    const dist = turf.distance(lastPath[lastPath.length - 1], path[0]) * 1000;


                    if (((enterDiffDiff > 110) || (exitDiffDiff > 110)) && (dist > 0)) {
                        console.log(journeyPatternRef, enterDiffDiff, exitDiffDiff, dist);
                        count++;
                        count2++;
                        path.shift();


                    } else {
                        done = true;
                        if (count) console.log("OK", count, journeyPatternRef, enterDiffDiff, exitDiffDiff, dist);
                    }
                }
            }*/

        }
        lastPath = path;
        spliced = spliced.concat(path);
        //spliced = dedupPath(spliced.concat(path));
    });

    if (spliced.length > 2) {
        removeKinks(spliced);
        const ls = turf.lineString(spliced)
        const options = { tolerance: 0.000001, highQuality: true };
        const simplified = turf.simplify(ls, options);



        if (count2)
            //console.log(JSON.stringify(simplified));

            return simplified.geometry.coordinates;
    } else return spliced;
}

function dedupPath(path) {

    const dedup = [];
    let lastPosition = [0, 0];
    path.forEach((position) => {
        if ((position[0] != lastPosition[0]) || (position[1] != lastPosition[1])) {
            dedup.push(position);
            lastPosition = position;
        }
        //else duplicates++;
    })
    return dedup;
}


function removeKinks(path, incompleteServiceLink) {

    const noKinks = [];
    lastPosition = null;
    let lastBearing = null;
    let lastDistance = null;
    let kinks = 0;
    //console.log(JSON.stringify(path))
    path.forEach((position) => {
        if (lastPosition) {
            let bearing = getBearing(lastPosition, position)
            const distance = turf.distance(lastPosition, position) * 1000;

            if (lastBearing) {

                let angleDiff = bearingDiff(bearing, lastBearing);

                if ((angleDiff > 110) && (distance > 50)) {
                    //if (!kinks) console.log("=======================!",path);
                    //console.log("distance: " + lastDistance + ">" + distance + " Bearing: " + lastBearing + ">" + bearing + "   Diff: " + angleDiff + "   " + incompleteServiceLink);
                    kinks++;
                }
                else {
                    if (kinks)
                        //console.log("OK distance: " + lastDistance + ">" + distance + " Bearing: " + lastBearing + ">" + bearing + "   Diff: " + angleDiff + "   " + incompleteServiceLink);
                        noKinks.push(position);
                    lastBearing = bearing;
                    lastPosition = position;
                    lastDistance = distance;
                }

            } else {
                lastBearing = bearing;
                lastPosition = position;
                lastDistance = distance;
            }
        } else lastPosition = position;


        //else duplicates++;
    })
    return noKinks;


}

function getBearing(lastPosition, position) {
    let bearing = turf.bearing(lastPosition, position);
    if (bearing < 0)
        bearing = 360 + bearing;
    return bearing;
}

function bearingDiff(bearing1, bearing2) {
    let lowBearing = bearing1;
    let highBearing = bearing2;
    if (lowBearing > highBearing) {
        const tempBearing = lowBearing;
        lowBearing = highBearing;
        highBearing = tempBearing;
    }
    let angleOverNorth = 360;
    if ((lowBearing < 180) && (highBearing > 180))
        angleOverNorth = (360 - highBearing) + lowBearing;

    let angleDiff = highBearing - lowBearing;

    if (angleOverNorth < angleDiff)
        angleDiff = angleOverNorth;


    return angleDiff;
}



async function etaMapper(states, message) {
    let result = [];

    let currentActualCallRef = message.value.EntityPartition.Key;
    const actualCallRecord = states.get("actualcall").get(currentActualCallRef);
    if (actualCallRecord) {
        if (actualCallRecord.status != "DONE") {
            const actualCall = {
                stateName: "actualcall",
                stateKey: currentActualCallRef,
                stateRecord: {
                    updateTimestamp: message.value.Timestamp,
                    etaUpdateTimestamp: message.value.Timestamp,
                    estimatedArrivalTime: message.value.EntityPartition.EstimatedTimeOfArrival,
                    estimatedDepartureTime: message.value.EntityPartition.EstimatedTimeOfDeparture,

                },
                merge: true
            }
            result.push(actualCall);
        }
        //progressActualCall(states, actualCall.key, message.value.EntityPartition.Status, message.value.Timestamp);

    }
    return result;
}

async function passMapper(states, message) {
    let result = [];
    if (message.value.EntityPartition.PointType == "STOPPOINT") {


        let currentActualCallRef = message.value.EntityPartition.ActualCallRef;
        const actualCallRecord = states.get("actualcall").get(currentActualCallRef);
        if (actualCallRecord) {

            result = progressActualCall(states, actualCallRecord.key, message.value.EntityPartition.Status, message.value.Timestamp);


            const actualCall = {
                stateName: "actualcall",
                stateKey: currentActualCallRef,
                stateRecord: {
                    updateTimestamp: message.value.Timestamp,
                    actualArrivalTime: message?.value?.EntityPartition?.Entrypoint?.Timestamp,
                    actualDepartureTime: message?.value?.EntityPartition?.ExitPoint?.Timestamp,
                },
                merge: true
            }
            result.unshift(actualCall);
        }
    }
    return result;
}

function progressActualCall(states, actualCallRef, statusEvent, timestamp) {
    let result = [];
    const actualCallRecord = states.get("actualcall").get(actualCallRef);
    if (actualCallRecord && (actualCallRecord?.status != "DONE")) {
        //actualCallRecord.updateTimestamp = timestamp;
        let status = null;
        let doneStatus = null;
        if (statusEvent == "APPROACHING") {
            status = "APPROACHING";
        } else if ((statusEvent == "ENTERED") && (!actualCallRecord.isLastInJourney)) {
            status = "ARRIVED";
        } else if ((statusEvent == "ENTERED") && (actualCallRecord.isLastInJourney)) {
            status = "DONE";
            doneStatus = "PASSED";
            if (actualCallRecord.previousActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.previousActualCallRef, "DETECTED", timestamp));
            if (actualCallRecord.nextActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.nextActualCallRef, "APPROACHING", timestamp));
        } else if (statusEvent == "EXITED" && (!actualCallRecord.isLastInJourney)) {
            status = "DONE";
            doneStatus = "PASSED";
            if (actualCallRecord.previousActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.previousActualCallRef, "DETECTED", timestamp));
            if (actualCallRecord.nextActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.nextActualCallRef, "APPROACHING", timestamp));
        } else if (statusEvent == "DETECTED") {
            status = "DONE";
            doneStatus = "DETECTED";
            if (actualCallRecord.previousActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.previousActualCallRef, "DETECTED", timestamp));
        } else if (statusEvent == "CANCELED") {
            status = "DONE";
            doneStatus = "CANCELED";
            if (actualCallRecord.previousActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.previousActualCallRef, "CANCELED", timestamp));
            if (actualCallRecord.nextActualCallRef) result = result.concat(progressActualCall(states, actualCallRecord.nextActualCallRef, "APPROACHING", timestamp));
        }


        if (doneStatus == "CANCELED") {
            result = result.concat(progressActualJourney(states, actualCallRecord.actualJourneyRef, "CANCELED", timestamp));

        } else if ((status == "DONE") && (actualCallRecord.isLastInJourney)) {
            result = result.concat(progressActualJourney(states, actualCallRecord.actualJourneyRef, "DONE", timestamp));

        } else {
            result = result.concat(progressActualJourney(states, actualCallRecord.actualJourneyRef, "RUNNING", timestamp));

        }

        const actualCall = {
            stateName: "actualcall",
            stateKey: actualCallRecord.key,
            stateRecord: {
                updateTimestamp: timestamp,
                statusTimestamp: timestamp,
                status: status,
                doneStatus: doneStatus,
            },
            merge: true
        }
        result.push(actualCall);

        //states.get("actualcall").set(actualCallRef, actualCall);
        //if (actualCall.vehicleRef == "200111") console.log(JSON.stringify(actualCall))
    }
    return result;
}

function progressActualJourney(states, actualJourneyRef, statusEvent, timestamp) {
    let result = [];
    const actualJourneyRecord = states.get("actualjourney").get(actualJourneyRef);
    if (actualJourneyRecord && ((actualJourneyRecord?.status != "DONE") || (actualJourneyRecord?.status != "CANCELED"))) {
        //actualCallRecord.updateTimestamp = timestamp;
        let status = null;
        let doneStatus = null;
        if (statusEvent == "RUNNING") {
            status = "RUNNING";
        } else if (statusEvent == "DONE") {
            status = "DONE";
            if (actualJourneyRecord.previousActualJourneyRef) result = result.concat(progressActualJourney(states, actualJourneyRecord.previousActualJourneyRef, "DONE", timestamp));
        } else if ((statusEvent == "CANCELED")) {
            status = "DONE";
            doneStatus = "CANCELED";
            if (actualJourneyRecord.previousActualJourneyRef) result = result.concat(progressActualJourney(states, actualJourneyRecord.previousActualJourneyRef, "CANCELED", timestamp));
            if (actualJourneyRecord.nextActualJourneyRef) result = result.concat(progressActualJourney(states, actualJourneyRecord.nextActualJourneyRef, "RUNNING", timestamp));
        }

        const actualBlockRecord = states.get("actualblock").get(actualJourneyRecord.actualBlockRef);
        if (actualJourneyRecord) {
            if (actualJourneyRecord.isLastInActualBlock && (status == "DONE")) {
                const actualBlock = {
                    stateName: "actualblock",
                    stateKey: actualJourneyRecord.actualBlockRef,
                    stateRecord: {
                        updateTimestamp: timestamp,
                        statusTimestamp: timestamp,
                        status: "DONE",
                        doneStatus: doneStatus,
                    },
                    merge: true
                }
                result.push(actualBlock);

            } else {
                const actualBlock = {
                    stateName: "actualblock",
                    stateKey: actualJourneyRecord.actualBlockRef,
                    stateRecord: {
                        updateTimestamp: timestamp,
                        statusTimestamp: timestamp,
                        status: "RUNNING",
                        doneStatus: null,
                    },
                    merge: true
                }
                result.push(actualBlock);
            }
        }

        const actualJourney = {
            stateName: "actualjourney",
            stateKey: actualJourneyRecord.key,
            stateRecord: {
                updateTimestamp: timestamp,
                statusTimestamp: timestamp,
                status: status,
                doneStatus: doneStatus,
            },
            merge: true
        }
        result.push(actualJourney);

        //states.get("actualcall").set(actualCallRef, actualCall);
        //if (actualCall.vehicleRef == "200111") console.log(JSON.stringify(actualCall))
    }
    return result;
}

async function entityMapper(states, message) {
    let result = [];
    //if (message.value)
    if (message.topic.match(/^(\w+\.){0,1}entity\.\w+\.\w+(\.\w+){0,1}$/))
        if (message?.value?.EntityPartition) {
            let timestamp = message?.value?.Timestamp;
            const entityName = message?.value?.Name.toLowerCase();
            const entityStateStore = states.get(entityName);
            if (entityStateStore) {




                let keyInBody = false;
                let key = message?.key;
                if ((!message?.value?.Key && !message?.value?.Key) && (message.value.EntityPartition.Key || message.value.EntityPartition.key)) keyInBody = true;

                let oldTimestamp = entityStateStore.get(key)?.timestamp

                if (oldTimestamp && (timestamp < oldTimestamp))
                    timestamp = oldTimestamp;

                const record = message.value.EntityPartition;

                if (keyInBody) {
                    delete message.value.EntityPartition.Key;
                    delete message.value.EntityPartition.key;
                }


                message.value.EntityPartition.key = key;
                message.value.EntityPartition.timestamp = timestamp;

                Object.entries(message.value.EntityPartition).forEach(([fieldKey, value]) => {
                    const propertyName = fieldKey.toCamelCase();
                    delete message.value.EntityPartition[fieldKey];
                    message.value.EntityPartition[propertyName] = value;
                });

                const entityState = {
                    stateName: entityName,
                    stateKey: key,
                    stateRecord: message.value.EntityPartition,
                    merge: true
                }


                result.push(entityState);
                if (entityState.stateRecord.lineRef) {
                    result.push({
                        stateName: "line",
                        stateKey: entityState.stateRecord.lineRef,
                        stateRecord: { key: entityState.stateRecord.lineRef, timestamp: timestamp },
                        merge: true
                    });
                }
                if (entityState.stateRecord.blockRef) {
                    result.push({
                        stateName: "block",
                        stateKey: entityState.stateRecord.blockRef,
                        stateRecord: { key: entityState.stateRecord.blockRef, timestamp: timestamp },
                        merge: true
                    });
                }
                if (entityState.stateRecord.quayRef) {
                    result.push({
                        stateName: "quay",
                        stateKey: entityState.stateRecord.quayRef,
                        stateRecord: { key: entityState.stateRecord.quayRef, timestamp: timestamp },
                        merge: true
                    });
                }
                if (entityState.stateRecord.journeyPatternRef) {
                    result.push({
                        stateName: "journeypattern",
                        stateKey: entityState.stateRecord.journeyPatternRef,
                        stateRecord: { key: entityState.stateRecord.journeyPatternRef, timestamp: timestamp },
                        merge: true
                    });
                }
                if (entityState.stateRecord.journeyRef) {
                    result.push({
                        stateName: "journey",
                        stateKey: entityState.stateRecord.journeyRef,
                        stateRecord: { key: entityState.stateRecord.journeyRef, timestamp: timestamp },
                        merge: true
                    });
                }
                if (entityState.stateRecord.vehicleRef) {
                    result.push({
                        stateName: "vehicle",
                        stateKey: entityState.stateRecord.vehicleRef,
                        stateRecord: { key: entityState.stateRecord.vehicleRef, timestamp: timestamp },
                        merge: true
                    });
                }
            }
        }
    /*else {
        result.push({
            stateName: message.stateName,
            stateKey: message.key,
            stateRecord: message.value.EntityPartition,
        })
    }*/

    return result;
}

function _isOlderThan(date, age) {
    const now = new Date();
    const then = new Date(date);
    const diff = now - date;
    if (diff > age) return true;
    else return false;
}

//const states = new State();

module.exports = State;