"use strict";

const Entity=require("./entity.js");

class ChainedEntity extends Entity {
    constructor(reference, properties, entityCollection, previousEntity, nextEntity) {
        super(reference, properties, entityCollection);
        this.nextEntity = null;
        this.previousEntity = null;
        //this.order=1;
        if (previousEntity) {
            previousEntity.insertAfter(this);
        }
        if (nextEntity) {
            nextEntity.insertBefore(this);
        }
    }

    delete(){
        this.removeEntityFromChain();
        super.delete();
    }

    insertBefore(chainedEntity) {
        if (chainedEntity) {
            if ((chainedEntity.previousEntity && this.previousEntity) || chainedEntity.nextEntity) throw ("Trying to insert entity that is inside an existing chain");
            else {
                if (this.previousEntity) {
                    chainedEntity.previousEntity = this.previousEntity;
                    this.previousEntity.nextEntity = chainedEntity;
                    this.previousEntity = chainedEntity;
                }
                this.previousEntity = chainedEntity;
                chainedEntity.nextEntity = this;
            }
        } else throw ("Trying to add nothing into existing chain");
    }

    insertAfter(chainedEntity) {
        if (chainedEntity) {
            if ((chainedEntity.nextEntity && this.nextEntity) || chainedEntity.previousEntity) throw ("Trying to insert entity that is inside an existing chain");
            else {
                if (this.nextEntity) {
                    chainedEntity.nextEntity = this.nextEntity;
                    this.nextEntity.previousEntity = chainedEntity;
                    this.nextEntity = chainedEntity;
                }
                this.nextEntity = chainedEntity;
                chainedEntity.previousEntity = this;
            }

        } else throw ("Trying to add nothing into existing chain");
    }

    removeEntityFromChain() {
        if (this.previousEntity) {
            this.previousEntity.nextEntity = this.nextEntity;
        }
        if (this.nextEntity) {
            this.nextEntity.previousEntity = this.previousEntity;
        }
        this.previousEntity = null;
        this.nextEntity = null;
    }

    isFirst() {
        if (this.previousEntity) return false;
        else return true;
    }

    isLast() {
        if (this.nextEntity) return false;
        else return true;
    }

    upstreamEvent(event) {
        if (!this.hasFinalState) {
            this.setState(event,false);
            if (this.previousEntity) this.previousEntity.upstreamEvent(event);
        }
    }

    downStreamEvent(event) {
        if (!this.hasFinalState) {
            this.setState(event,false);
            if (this.nextEntity) this.nextEntity.downStreamEvent(event);
        }
    }
}



module.exports = ChainedEntity;