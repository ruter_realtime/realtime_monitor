"use strict";

const HierarchicalChainedEntity = require("./hierarchicalChainedEntity.js");

class ActuallCall extends HierarchicalChainedEntity {
    constructor(reference, properties, entityCollection, previousEntity, nextEntity,parentEntity){
        super(reference, properties, entityCollection, previousEntity, nextEntity,parentEntity);
        this.setState("ASSIGNED",false);
    }

    downstreamEvent(event) {
        if (!this.hasFinalState) {
            if (event == "APPROACHING") this.setState("APPROACHING", false);
        }
    }

    upstreamEvent(event) {
        if (!this.hasFinalState) {
            let upstreamEvent = null;
            let downstreamEvent=null;

            if ((event == "ENTERED") && this.isLast()) {
                upstreamEvent = "DETECTED";
                downstreamEvent="APPROACHING";
                this.setState("PASSED", true);
            } else if ((event == "ENTERED")) {
                upstreamEvent = "DETECTED";
                this.setState("ARRIVED", false);
            }else if (event == "EXITED") {
                upstreamEvent = "DETECTED";
                downstreamEvent="APPROACHING";
                this.setState("PASSED", true);
            } else if (event == "PASSED") {
                upstreamEvent = "DETECTED";
                downstreamEvent="APPROACHING";
                this.setState("PASSED", true);
            } else if (event == "DETECTED") {
                upstreamEvent = "DETECTED";
                this.setState("DETECTED", true);
            } else if (event == "CANCELED") {
                upstreamEvent = "UPSTREAM_CANCELED";
                downstreamEvent="APPROACHING";
                this.setState("CANCELED", true);
            } else if (event == "UPSTREAM_CANCELED") {
                upstreamEvent = "UPSTREAM_CANCELED";
                this.setState("CANCELED", true);
            }
           if(upstreamEvent&&this.previousEntity) this.previousEntity.upstreamEvent(upstreamEvent);
           if(downstreamEvent&&this.nextEntity) this.nextEntity.downstreamEvent(downstreamEvent);
        }
    }
}
const parent = new ActuallCall(1, null, null);


const col = new Map();
const first = new ActuallCall(1, null, col,null,null,parent);
const second = new ActuallCall(2, null, col, first);
const third = new ActuallCall(3, null, col, second);
const forth = new ActuallCall(4, null, col,third);



first.upstreamEvent("PASSED");
//second.upstreamEvent("HEI");
third.upstreamEvent("EXITED");
console.log(first.get());
forth.upstreamEvent("CANCELED");
module.exports = ActuallCall;