"use strict";

const ChainedEntity = require("./chainedEntity.js");

class HierarchicalChainedEntity extends ChainedEntity {
    constructor(reference, properties, entityCollection, previousEntity, nextEntity, parentEntity) {
        super(reference, properties, entityCollection, previousEntity, nextEntity);
        this.parentEntity = null;
        this.childEntities = new Map();
        this.firstChildEntity = null;
        this.lastChildEntity = null;
        if (parentEntity) {
            this.setParent(parentEntity);
        } else if(this?.previousEntity?.parentEntity) this.setParent(this.previousEntity.parentEntity);
        else if(this?.nextEntity?.parentEntity) this.setParent(this.nextEntity.parentEntity);

    }

    setParent(parentEntity) {
        _throwErrorIfNotInitialized(this, "Trying to set parent on an un-initialized entity");
        if (!this.parentEntity) {
            parentEntity.addChild(this);
        } else throw ("Parent entity is set and can not be changed");
    }

    addChild(childEntity) {
        _throwErrorIfNotInitialized(this, "Trying to add child on an un-initialized entity");
        if (childEntity.parentEntity) throw ("Child entity has parent set and can not be changed");
        if (childEntity?.nextEntity&&(childEntity?.nextEntity?.parentEntity != this)) throw ("Child entity sibling has other or no parent set");
        if (childEntity?.previousEntity&&(childEntity?.previousEntity?.parentEntity != this)) throw ("Child entity sibling has other or no parent set");

        this.childEntities.set(childEntity.key, childEntity);
        childEntity.parentEntity = this;
        if (!childEntity?.previousEntity && !childEntity?.nextEntity)
            if (this.lastChildEntity) lastChildEntity.insertAfter(childEntity)
    }

    removeChild(childEntity) {
        _throwErrorIfNotInitialized(this, "Trying to remove child from an un-initialized entity");
        if (childEntity.parentEntity != this) throw ("Child entity has other or no parent set and can not be removed");

        childEntities.delete(childEntity.key);
        childEntity.parentEntity = null;
        childEntity.removeEntityFromChain();
    }


    delete() {
        _throwErrorIfNotInitialized(this, "Trying to delete into an un-initialized entity");
        if (this.parentEntity) this.parentEntity.removeChild(this);

        this.childEntities = new Map();
        this.firstChildEntity = null;
        this.lastChildEntity = null;
        super.delete();
    }

    insertBefore(chainedEntity) {
        _throwErrorIfNotInitialized(this, "Trying to insert into an un-initialized entity");
        if (chainedEntity.parentEntity) throw ("Insert entity has parent set and can not be changed");
        chainedEntity.parentEntity=this.parentEntity;
        super.insertBefore(chainedEntity);
    }

    insertAfter(chainedEntity) {
        _throwErrorIfNotInitialized(this, "Trying to insert into an un-initialized entity");
        if (chainedEntity.parentEntity) throw ("Insert entity has parent set and can not be changed");
        chainedEntity.parentEntity=this.parentEntity;
        super.insertAfter(chainedEntity);
    }

    removeEntityFromChain() {
        _throwErrorIfNotInitialized(this, "Trying to remove an un-initialized entity from chain");
        if (this.parentEntity) this.parentEntity.removeChild(this);
        else super.removeEntityFromChain();
    }


}

function _throwErrorIfNotInitialized(context, message) {
    if (!context.initialized) throw (message);
}


module.exports = HierarchicalChainedEntity;